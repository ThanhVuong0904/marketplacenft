const NFT = artifacts.require("NFTCollection");
const Marketplace = artifacts.require("Marketplace");


contract('Marketplace', accounts => {
     let nft
     let marketplace
     beforeEach(async () => {
          marketplace = await Marketplace.new()
          nft = await NFT.new()
          await nft.createNFT('string uri', 0, {from: accounts[0]})
          await nft.createNFT('string uri 2', 0, {from: accounts[0]})
          await nft.createNFT('string uri 3', 0, {from: accounts[0]})
     })
     it.skip('should add and buy item', async () => {
          await nft.setApprovalForAll(marketplace.address, true)
          await marketplace.addItemToMarket(1, nft.address, '1000000000000000000', {from: accounts[0]})
          const itemforSale = await marketplace._item(0)
          console.log("token id",itemforSale.tokenId.toString())
          console.log("token address",itemforSale.tokenAddress)
          console.log("seller",itemforSale.seller)
          console.log("askingPrice",itemforSale.askingPrice.toString())
          console.log("is Sold",itemforSale.isSold)

          const list = await marketplace._marketId()
          console.log("list id", list.toString());
          await marketplace.buyItem(nft.address, 1, {from: accounts[1], value: '1000000000000000000'})
          
          await nft.setApprovalForAll(marketplace.address, true, {from: accounts[1]})
          await marketplace.addItemToMarket(2, nft.address, '2000000000000000000', {from: accounts[1]})
          const list2 = await marketplace._marketId()
          console.log("list id", list2.toString());
          console.log("-----------------");
          const itemforSale1 = await marketplace._item(0)
          console.log("token id",itemforSale1.tokenId.toString())
          console.log("token address",itemforSale1.tokenAddress)
          console.log("seller",itemforSale1.seller)
          console.log("askingPrice",itemforSale1.askingPrice.toString())
          console.log("is Sold",itemforSale1.isSold)
     })
     it.skip('should get', async () => {
          await nft.setApprovalForAll(marketplace.address, true)
          await marketplace.addItemToMarket(1, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.addItemToMarket(2, nft.address, '1000000000000000000', {from: accounts[0]})

          const get = await marketplace.getItem()
          console.log(get);
     })
     it.skip('should cancel item', async () => {
          await nft.setApprovalForAll(marketplace.address, true)
          await marketplace.addItemToMarket(1, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.cancel(nft.address, 1)
          await marketplace.addItemToMarket(2, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.addItemToMarket(3, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.buyItem(nft.address, 2, {from: accounts[1], value: '1000000000000000000'})


          // await marketplace.buyItem(nft.address, 1, {from: accounts[1], value: '1000000000000000000'})

          const get = await marketplace.getItem()
          console.log(get);
     })
     it.only('should add', async () => {
          await nft.setApprovalForAll(marketplace.address, true)
          await marketplace.addItemToMarket(1, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.addItemToMarket(2, nft.address, '1000000000000000000', {from: accounts[0]})
          await marketplace.updateItem(2, nft.address, '5000000000000000000', {from: accounts[0]})
          const get = await marketplace.getItem()
          console.log(get);
     })
})