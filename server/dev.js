require('dotenv').config();
const https = require("https");
const fs = require("fs");
//Express
const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const AccountRouter = require('./routes/AccountRouter')
const NFTRouter = require('./routes/NFTRouter')
const MarketRouter = require('./routes/MarketRouter')
const AssetRouter = require('./routes/AssetRouter')
const TokenRouter = require('./routes/TokenRouter')

//Start
const app = express();

app.use(cors());
app.use(
    fileUpload({
        useTempFiles: true,
    }),
);
app.use(express.json({limit: '50mb'}));


const PORT = process.env.PORT || 8000;
const options = {
    key: fs.readFileSync("./ssl/server.key"),
    cert: fs.readFileSync("./ssl/server.crt"),
};
const sslServer = https.createServer(options, app);
sslServer.listen(PORT, () => console.log(`Server started on port ${PORT}`));

app.use('/account', AccountRouter)
app.use('/nft', NFTRouter)
app.use('/market', MarketRouter)
app.use('/asset', AssetRouter)
app.use('/token', TokenRouter)