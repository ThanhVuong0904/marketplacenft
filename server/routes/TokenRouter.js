const express = require('express')
const router = express.Router()

const TokenController = require('../controllers/TokenController')

router.post('/transfer', TokenController.transfer)


module.exports = router