const express = require('express')
const router = express.Router()

const MarketController = require('../controllers/MarketController')

router.get('/all-nft', MarketController.getAlls)
router.post('/add', MarketController.add)
router.post('/buy', MarketController.buy)
router.post('/cancel', MarketController.cancel)

module.exports = router