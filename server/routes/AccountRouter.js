const express = require('express')
const router = express.Router()

const AccountController = require('../controllers/AccountController')

router.get('/native/:address', AccountController.getNativeBalance)
router.get('/:accountAddress/nft/:tokenAddress', AccountController.getNFTs)
router.get('/create-wallet', AccountController.createWallet)

module.exports = router