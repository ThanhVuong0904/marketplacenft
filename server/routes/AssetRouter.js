const express = require('express')
const router = express.Router()

const AssetController = require('../controllers/AssetController')

router.post('/upload', AssetController.upload)

module.exports = router