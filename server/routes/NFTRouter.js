const express = require('express')
const router = express.Router()

const NFTController = require('../controllers/NFTController')

router.post('/transfer', NFTController.transfer)
router.post('/create-metadata', NFTController.createMetadata)
router.post('/mint', NFTController.createNFT)


module.exports = router