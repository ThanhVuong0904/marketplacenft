const Moralis = require('moralis/node')

class MoralisModel {
    constructor() {
        this.init()
        this.service = Moralis
    }
    async init() {
        await Moralis.start({
            serverUrl: process.env.SERVER_URL,
            appId: process.env.APP_ID,
            masterKey: process.env.MASTER_KEY,
        });
    }
}

module.exports = MoralisModel