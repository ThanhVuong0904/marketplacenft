const Web3 = require('web3');

class Web3Model {
    constructor() {
        this.web3Mumbai = new Web3('https://rpc-mumbai.maticvigil.com/v1/6b777edf69ecfce2e7b19154f357db23406b0484');
        this.web3ETH = new Web3('https://rinkeby.infura.io/v3/59c1b072fbfb4b32adb7a6f332267f4d');
        this.web3Bsc = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545')
    }
}

module.exports = Web3Model