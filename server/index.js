require('dotenv').config();

const express = require('express')
const fileUpload = require("express-fileupload");
const Web3 = require("web3")
const cloudinary = require('cloudinary')
const cors = require('cors')
const Moralis = require('moralis/node')
const serverUrl = "https://i39qsbxfsywb.usemoralis.com:2053/server";
const appId = "aIQPRg4dNbnp6WXxriv8vf0sCt8v54hwmHKamuXE";
const masterKey = 'GQwMSOoVAAHapcyDgL62sS5qu0k0WBDhC89mNo1w'
const NFTAPI = require("./abiNFT.js");
const MARKETABI = require("./abiMarket.js");
const { default: axios } = require('axios');
const https = require('https');
const fs = require('fs');
const TOKEN_CONTRACT_ADDRESS_RINKEBY = '0x177ba61bb06BfDcE5B62FaC85566CF5bfA3904b7'
const TOKEN_CONTRACT_ADDRESS_BSC = '0x1c6306F7022D4634CBe3e369cdc9309194F63F37'
const MARKET_RINKEBY_ADDRESS_RINKEBY = '0x7DdD4B1aE86B63627ff8f37708C9a54D88dc933B'
const MARKET_RINKEBY_ADDRESS_BSC = '0x36De268457A290C1f10b1Adb2CD115A447D2cC03'
const app = express()
app.use(express.json())
app.use(cors())
app.use(fileUpload({
     useTempFiles: true
}))
const options = {
     key: fs.readFileSync('server.key'),
     cert: fs.readFileSync('server.crt')
};
cloudinary.config({
     cloud_name: process.env.CLOUD_NAME,
     api_key: process.env.CLOUD_API_KEY,
     api_secret: process.env.CLOUD_API_SECRET,
})
const sslServer = https.createServer(options, app)
const PORT = process.env.PORT || 8000
sslServer.listen(PORT, () => console.log(`Server started on port ${PORT}`))

let web3ETH;
let web3Bsc;
let contractNFT;
let contractMarket;
let contractNFTBsc;
let contractMarketBsc;
const init = async () => {
     await Moralis.start({ serverUrl, appId, masterKey });
     console.log("Moralis Start");
     web3ETH = new Web3('https://rinkeby.infura.io/v3/59c1b072fbfb4b32adb7a6f332267f4d');
     web3Bsc = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545');

     contractNFT = await new web3ETH.eth.Contract(NFTAPI, TOKEN_CONTRACT_ADDRESS_RINKEBY)
     contractMarket = await new web3ETH.eth.Contract(MARKETABI, MARKET_RINKEBY_ADDRESS_RINKEBY)
     console.log("Init NFT and Market Rinkeby");
     contractNFTBsc = await new web3Bsc.eth.Contract(NFTAPI, TOKEN_CONTRACT_ADDRESS_BSC)
     contractMarketBsc = await new web3Bsc.eth.Contract(MARKETABI, MARKET_RINKEBY_ADDRESS_BSC)
     console.log("Init NFT and Market BSC");
}
init()

app.post('/create-nft', async (req, res) => {
     const {
          account, 
          private_key, 
          metamask, 
          name, 
          description,
          flagImage, 
          image, 
          video,
          network,
     } = req.body     
     const parseMetamask = JSON.parse(metamask)
     const parseFlag = flagImage && JSON.parse(flagImage)
     const parseIsUploadImage = image && JSON.parse(image)
     const parseIsUploadVideo = video && JSON.parse(video)
     const parseName = name && JSON.parse(name)
     const parseDescription = description && JSON.parse(description)
     const parseNetwork = network && JSON.parse(network)
     const uploadImageCloudinary = async () => {
          let url
          await cloudinary.v2.uploader.upload(req.files.fileImage.tempFilePath, {folder: "upload_image"}, async(err, result) => {
               if(err) throw err
               url = result.secure_url
               console.log(result.secure_url);
               return url
          })
          return url
     }
     const createMetadata = async () => {
          let metadata;
          if(parseFlag === true) {
               if(!parseIsUploadImage.isUpload) {
                    //Image URL
                    console.log("Image Url");
                    metadata = {
                         name: parseName,
                         image: parseIsUploadImage.url,
                         description: parseDescription
                    }
               }
               //User uploadImage
               else {
                    console.log(parseName);
                    console.log("NFT Image upload")
                    const file = req.files.fileImage
                    if(file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
                         return res.status(400).json({success: false, message: "File format is incorrect"})
                    }
                    const url = await uploadImageCloudinary()
                    metadata = {
                         name: parseName,
                         image: url,
                         description: parseDescription
                    }
               }
          }
          else {
               //Youtute URL, Image Url
               if(parseIsUploadVideo.isYoutube && !parseIsUploadVideo.image.isUpload) {
                    console.log("video URL, Image URL");
                    metadata = {
                         name: parseName,
                         description: parseDescription,
                         image: parseIsUploadVideo.image.url,
                         external_url: parseIsUploadVideo.url,
                         animation_url: parseIsUploadVideo.url,
                    }
               }
               //Youtube, Image Upload
               if(parseIsUploadVideo.isYoutube && parseIsUploadVideo.image.isUpload) {
                    console.log("Video URL, Image Upload");
                    const file = req.files.fileImage
                    console.log(req.files);
                    // if(file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') {
                    //      return res.status(400).json({success: false, message: "File format is incorrect"})
                    // }
                    const url = await uploadImageCloudinary()
                    console.log(url);
                    metadata = {
                         name: parseName,
                         image: url,
                         description: parseDescription,
                         external_url: parseIsUploadVideo.url,
                         animation_url: parseIsUploadVideo.url,
                    }
               }
          }
          const nftFileMetadataFile = new Moralis.File(
               "metadata.json", 
               {
                    base64 : Buffer.from(JSON.stringify(metadata)).toString("base64")
               }
          );
          await nftFileMetadataFile.saveIPFS({useMasterKey: true});
          const nftFileMetadataFilePath = nftFileMetadataFile.ipfs();
          console.log("metadata",nftFileMetadataFilePath);
          return nftFileMetadataFilePath 
     }
     
     if(parseMetamask === true) {
          const metadata = await createMetadata()
          return res.json({
               success: true,  
               message: `Create Metadata for mint NFT`, 
               metadata
          })
     }
     else {
          let tx
          const metadata = await createMetadata()
          if(parseNetwork === 'rinkeby') {
               const balance = await web3ETH.eth.getBalance(account)
               if(balance < "10000000000000000") {
                    return res.json({
                         success: false,
                         message: "You must have 0.01 ether for CreateNFT",
                         yourEther: web3ETH.utils.fromWei(balance, 'ether')
                    })
               }
               web3ETH.eth.accounts.wallet.add(private_key)
               web3ETH.eth.handleRevert = true
               console.log("rinkeby");
               const gasPrice = await contractNFT.methods.createNFT(metadata,0).estimateGas({from: account, value: "10000000000000000"})
               console.log(gasPrice);
               tx = await contractNFT.methods.createNFT(metadata, 0)
               .send({from: account, gas: gasPrice, value: "10000000000000000"})
               .catch(error => res.json({success: false, message: error.reason}))
          }
          if(parseNetwork === 'bsc testnet') {
               console.log("bsc testnet");
               const balance = await web3Bsc.eth.getBalance(account)
               if(balance < "10000000000000000") {
                    return res.json({
                         success: false,
                         message: "You must have 0.01 ether for CreateNFT",
                         yourEther: web3ETH.utils.fromWei(balance, 'ether')
                    })
               }
               web3Bsc.eth.accounts.wallet.add(private_key)
               web3Bsc.eth.handleRevert = true
               const gasPrice = await contractNFTBsc.methods
                    .createNFT(metadata,0)
                    .estimateGas({from: account, value: "10000000000000000"})
               console.log(gasPrice);
               tx = await contractNFTBsc.methods.createNFT(metadata,0)
               .send({from: account, gas: gasPrice, gasPrice: '20000000000', value: "10000000000000000"})
               .catch(error => {
                    // console.log(error);
                    return res.json({success: false, message: error.reason})
               })
          }

          return res.json({
               success: true,  
               message: `Create NFT with ${parseFlag === true ? "Image" : "Video"} and network ${parseNetwork}`, 
               tokenId: tx.events.CreateNFT.returnValues.tokenId,
               metadata, tx,
          })
     }
})
app.post('/add-marketplace', async (req, res) => {
     const {account, private_key, token_id, network, asking_price} = req.body
     const checkIsApproveForAll = async (_network) => {
          if(_network === 'rinkeby') {
               const isApproveForAll = await contractNFT.methods.isApprovedForAll(account, MARKET_RINKEBY_ADDRESS_RINKEBY).call()
               return isApproveForAll
          }
          if(_network === 'bsc testnet') {
               const isApproveForAll = await contractNFTBsc.methods.isApprovedForAll(account, MARKET_RINKEBY_ADDRESS_BSC).call()
               return isApproveForAll
          }
     }
     // let gasApprove
     if(network === 'rinkeby') {
          console.log('call rinkeby');
          const ownerOf = await contractNFT.methods.ownerOf(token_id).call({from: account})
          console.log({ownerOf});
          console.log({account});
          if(account !== ownerOf) {
               return res.json({success: false, message: `You are not the owner for NFT Token Id = ${token_id}`})
          }
          const isApproveForAll = await checkIsApproveForAll(network);
          console.log(isApproveForAll);
          if(!isApproveForAll) {
               let gasApprove = await contractNFT.methods.setApprovalForAll(MARKET_RINKEBY_ADDRESS_RINKEBY, true)
                    .estimateGas({from: account})
               console.log({gasApprove});
               await contractNFT.methods.setApprovalForAll(MARKET_RINKEBY_ADDRESS_RINKEBY, true)
               .send({from: account, gas: gasApprove})
               .catch(error => {
                    console.log(error);
                    return res.json({success: false, message: error.reason})
               })
               const isApproveForAll = await checkIsApproveForAll(network);
               console.log(isApproveForAll);
          }
          web3ETH.eth.accounts.wallet.add(private_key)
          
          web3ETH.eth.handleRevert = true
          const gasAddItemToMarket = await contractMarket.methods.addItemToMarket(token_id, TOKEN_CONTRACT_ADDRESS_RINKEBY, asking_price)
               .estimateGas({from: account})
          await contractMarket.methods.addItemToMarket(token_id, TOKEN_CONTRACT_ADDRESS_RINKEBY, asking_price)
          .send({from: account, gasPrice: '20000000000', gas: gasAddItemToMarket})
          .catch(error => {
               console.log(error);
               return res.json({success: false, message: error.reason})
          })
     }
     if(network === 'bsc testnet') {
          web3Bsc.eth.accounts.wallet.add(private_key)
          web3Bsc.eth.handleRevert = true
          const ownerOf = await contractNFTBsc.methods.ownerOf(token_id).call()
          console.log(ownerOf);
          if(account !== ownerOf) {
               return res.json({success: false, message: `You are not the owner for NFT Token Id = ${token_id}`})
          }
          const isApproveForAll = await checkIsApproveForAll(network);
          console.log(isApproveForAll);
          if(!isApproveForAll) {
               let gasApprove = await contractNFTBsc.methods.setApprovalForAll(MARKET_RINKEBY_ADDRESS_BSC, true)
                    .estimateGas({from: account})
               
               console.log({gasApprove});
               await contractNFTBsc.methods.setApprovalForAll(MARKET_RINKEBY_ADDRESS_BSC, true)
                    .send({from: account, gasPrice: '20000000000', gas: gasApprove})
                    .catch(error => {
                         console.log(error);
                         return res.json({success: false, message: error.reason})
                    })
               const a = await checkIsApproveForAll(network);
               console.log("2",a);
          }
          
          const gasAddItemToMarket = await contractMarketBsc.methods.addItemToMarket(token_id, TOKEN_CONTRACT_ADDRESS_BSC, asking_price)
               .estimateGas({from: account})
          await contractMarketBsc.methods.addItemToMarket(token_id, TOKEN_CONTRACT_ADDRESS_BSC, asking_price)
               .send({from: account, gasPrice: '20000000000', gas: gasAddItemToMarket})
               .catch(error => {
                    console.log(error);
                    return res.json({success: false, message: error.reason})
               })
     }

     return res.json({
          success: true, 
          message: `Add item to market place with network ${network}`, 
     })
})
app.post('/get-NFT-owner', async (req, res) => {
     const {account, network} = req.body
     // const options = {
     //      address: network === 'rinkeby' ? TOKEN_CONTRACT_ADDRESS_RINKEBY : TOKEN_CONTRACT_ADDRESS_BSC,
     //      chain: network,
     // };
     const options = {
          address: TOKEN_CONTRACT_ADDRESS_RINKEBY,
          chain: 'rinkeby',
     };
     const nftOwners = await Moralis.Web3API.token.getNFTOwners(options);
     console.log(nftOwners);
     const nfts = nftOwners.result.find(item => item.token_address === TOKEN_CONTRACT_ADDRESS_RINKEBY || item.token_address === TOKEN_CONTRACT_ADDRESS_BSC)
     return res.json({success: true, message: 'Get all nft owner', nft: nfts})
})
app.post('/cancel-marketplace', async (req, res) => {
     const {account, private_key, token_id, network} = req.body

     if(network === 'rinkeby') {
          web3ETH.eth.accounts.wallet.add(private_key)
          const gasCancel = await contractMarket.methods.cancel(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
               .estimateGas({from: account})
          await contractMarket.methods.cancel(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
          .send({from: account, gas: gasCancel})
     }

     if(network === 'bsc testnet') {
          web3Bsc.eth.accounts.wallet.add(private_key)
          const gasCancel = await contractMarketBsc.methods.cancel(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
               .estimateGas({from: account})
          await contractMarketBsc.methods.cancel(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
          .send({from: account, gas: gasCancel})
     }

     return res.json({
          success: true, 
          message: `Cancel sell item ${network}`, 
     })
})

app.post('/buy-marketplace', async (req, res) => {
     const {account, private_key, token_id, network, price} = req.body
     if(network === 'rinkeby') {
          console.log(price);
          web3ETH.eth.accounts.wallet.add(private_key)
          web3ETH.eth.handleRevert = true
          let gasApproveMarket = await contractMarket.methods.buyItem(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
          .estimateGas({from: account})
          await contractMarket.methods.buyItem(TOKEN_CONTRACT_ADDRESS_RINKEBY, token_id)
          .send({from: account, gasPrice: '2000000000', gas: gasApproveMarket, value: price})
          .catch(error => res.json({success: false, message: error.reason}))
          //stop here
     }
     if(network === 'bsc testnet') {
          console.log(price);
          web3Bsc.eth.accounts.wallet.add(private_key)
          web3Bsc.eth.handleRevert = true
          let gasApproveMarket = await contractMarketBsc.methods.buyItem(TOKEN_CONTRACT_ADDRESS_BSC, token_id)
          .estimateGas({from: account})
          await contractMarketBsc.methods.buyItem(TOKEN_CONTRACT_ADDRESS_BSC, token_id)
          .send({from: account, gasPrice: '2000000000', gas: gasApproveMarket, value: price})
          .catch(error => res.json({success: false, message: error.reason}))
          //stop here
     }
     let item = {
          tokenId: token_id,
          tokenAddress: token_address,
     }
     return res.json({
          success: true, 
          message: `Buy item network ${network}`, 
          marketItem: item
     })
})
app.get('/all-nft-marketplace', async (req, res) => {
     let txRinkeby = await contractMarket.methods.getItems().call()
     let itemRinkeby = txRinkeby.map(i => {
          return {
               tokenId: i.tokenId,
               tokenAddress: i.tokenAddress,
               seller: i.seller,
               askingPrice: i.askingPrice,
               isSold: i.isSold
          }
     })
     let txBsc = await contractMarketBsc.methods.getItems().call()
     let itemBsc = txBsc.map(i => {
          return {
               tokenId: i.tokenId,
               tokenAddress: i.tokenAddress,
               seller: i.seller,
               askingPrice: i.askingPrice,
               isSold: i.isSold
          }
     })
     return res.json({
          success: true, 
          message: 'Get all NFT in market place', 
          marketPlaceRinkeby: itemRinkeby,
          marketPlaceBsc: itemBsc,
     })
})

