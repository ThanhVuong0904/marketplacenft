const ethers = require('ethers')


const Web3Model = require('../models/Web3Model')
const MoralisModel = require('../models/MoralisModel')

const {SUPPORT_NETWORK, SUPPORT_BLOCKCHAIN} = require('../constants')

const AccountController = {
    getNativeBalance: async (req, res) => {
        const {address} = req.params
        const {blockchain, network} = req.query

        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})

        const web3Model = new Web3Model()
        try {
            let balance
            if(blockchain === 'ETH') {
                if(network === 'rinkeby') {
                    balance = await web3Model.web3ETH.eth.getBalance(address)
                }
                else {
                    return res.json({success: false, message: `Only support network rinkeby`})
                }
            }

            if(blockchain === 'BSC') {
                if(network === 'bsc testnet') {
                    balance = await web3Model.web3Bsc.eth.getBalance(address)
                }
                else {
                    return res.json({success: false, message: `Only support network bsc testnet`})
                }
            }

            if(blockchain === 'Polygon') {
                if(network === 'mumbai') {
                    balance = await web3Model.web3Mumbai.eth.getBalance(address)
                }
                else {
                    return res.json({success: false, message: `Only support network mumbai`})
                }
            }
            return res.json({
                success: true,
                message: 'Get your native balance', 
                balance,
                address,
                blockchain,
                network,
            })
        } catch (err) {
            res.status(500).json({ success: false, message: 'Something went wrong', err });
        }
    },
    getNFTs: async (req, res) => {
        const {tokenAddress, accountAddress} = req.params
        const {blockchain, network} = req.query
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})

        let _chain
        if(blockchain === 'ETH') {
            if(network === 'rinkeby') {
                _chain = '0x4'
            }
            else {
                return res.json({success: false, message: `Only support network rinkeby`})
            }
        }

        if(blockchain === 'BSC') {
            if(network === 'bsc') {
                _chain = '0x61'
            }
            else {
                return res.json({success: false, message: `Only support network bsc testnet`})
            }
        }

        if(blockchain === 'Polygon') {
            if(network === 'mumbai') {
                _chain = 'mumbai'
            }
            else {
                return res.json({success: false, message: `Only support network mumbai`})
            }
        }

        try {
            const moralisModel = new MoralisModel()
            const options = {
                chain: _chain,
                address: accountAddress,
                token_address: tokenAddress,
            };
            const nfts = await moralisModel.service.Web3API.account.getNFTsForContract(options);
            return res.json({
                success: true, 
                message: 'Get NFTs owner', 
                nfts: nfts.result,
                blockchain,
                network,
            })
        } catch (error) {
            console.log(error);
            res.status(500).json({ success: false, message: 'Something went wrong', err });
        }
        
    },
    createWallet: async (req, res) => {
        try {
            const wallet = ethers.Wallet.createRandom()
            const _signingKey = wallet._signingKey()
            const _mnemonic = wallet._mnemonic()
            return res.json({
                success: true,
                message: 'Create wallet',
                wallet: {
                    address: wallet.address,
                    private_key: _signingKey.privateKey,
                    mnemonic: _mnemonic.phrase
                }
            })
        } catch (error) {
            console.log(error);
            return res.status(500).json({ success: false, message: 'Something went wrong', err }); 
        }
    }
}

module.exports = AccountController