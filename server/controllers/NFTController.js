const Web3Model = require('../models/Web3Model')
const MoralisModel = require('../models/MoralisModel')

const { 
    SUPPORT_NETWORK, SUPPORT_BLOCKCHAIN, 
    NFT_MUMBAI_ADDRESS, NFT_RINKEBY_ADDRESS, NFT_BSC_TESTNET_ADDRESS 
} = require('../constants')
const { ETH_BSC_ERC1155_ABI, POLYGON_ERC1155_ABI, ERC721_ABI } = require('../abi/NFT')

//Ví tổng MUMBAI
const ADMIN = process.env.ACCOUNT_ADDRESS;
const KEY_ADMIN = process.env.PRIVATE_KEY;
const GAS_LIMIT = '10000000000'

const NFTController = {
    transfer: async (req, res) => {
        const {tokenAddress, tokenId, nftType, from, to, amount, blockchain, network} = req.body
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }
        if(tokenAddress === undefined || tokenId === undefined || nftType === undefined) {
            return res.json({success: false, message: 'Token address, Token id, NFT type is required'})
        }
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})

        const web3Model = new Web3Model()

        const transferNFT1155 = async (web3, tokenAddress) => {
            web3.eth.accounts.wallet.add(from.privateKey);
            const contract  = new web3.eth.Contract(ETH_BSC_ERC1155_ABI, tokenAddress);

            const gasEstimated = await contract.methods.safeTransferFrom(
                from.accountAddress, 
                to, 
                tokenId, 
                amount, 
                [] //data byte32
            ).estimateGas({ from: from.accountAddress });

            const tx = await contract.methods.safeTransferFrom(
                from.accountAddress, 
                to, 
                tokenId, 
                amount, 
                []
            ).
            send({ 
                from: from.accountAddress, 
                gas: Math.ceil(gasEstimated, 1.1), 
                gasLimit: GAS_LIMIT 
            });
            return tx
        }
        const transferNFT721= async (web3, tokenAddress) => {
            web3.eth.accounts.wallet.add(from.privateKey);
            const contract  = new web3.eth.Contract(ERC721_ABI, tokenAddress);
            //Caculate Price
            const gasEstimated = await contract.methods.safeTransferFrom(
                from.address, 
                to, 
                tokenId
            ).estimateGas({ from: from.accountAddress });

            const tx = await contract.methods.safeTransferFrom(
                from.accountAddress, 
                to, 
                tokenId
            )
            .send({ 
                from: from.accountAddress, 
                gas: Math.ceil(gasEstimated, 1.1), 
                gasLimit: GAS_LIMIT 
            });
            return tx
        }
        
        if(blockchain === "ETH") {
            switch(network) {
                case 'rinkeby':
                    try {
                        let tx
                        if(nftType === 'ERC721') {
                            tx = await transferNFT721(web3Model.web3ETH, tokenAddress)
                            
                        }
                        if(nftType === 'ERC1155') {
                            tx = await transferNFT1155(web3Model.web3ETH, tokenAddress)
                        }

                        return res.json({
                            success: true, message: `Transfer NFT`,
                            token_id: tokenId,
                            token_address: tokenAddress,
                            nft_type: nftType,
                            from: from.accountAddress,
                            to,
                            amount,
                            blockchain,
                            testnet: network,
                            transaction_hash: tx.transactionHash,
                            gas_used: tx.gasUsed
                        })
                    } catch (error) {
                        console.log(error)
                        return res.json({success: false, message: error})
                    }
                default: return res.json({success: false, message: 'Not support network'})
            }
        }
        if(blockchain === "BSC") {
            switch(network) {
                case 'bsc testnet':
                    try {
                        let tx
                        if(nftType === 'ERC721') {
                            tx = await transferNFT721(web3Model.web3Bsc, tokenAddress)
                        }
                        if(nftType === 'ERC1155') {
                            tx = await transferNFT1155(web3Model.web3Bsc, tokenAddress)
                        }
                        return res.json({
                            success: true, message: `Transfer NFT`,
                            token_id: tokenId,
                            token_address: tokenAddress,
                            nft_type: nftType,
                            from: from.accountAddress,
                            to,
                            amount,
                            blockchain,
                            testnet: network,
                            transaction_hash: tx.transactionHash,
                            gas_used: tx.gasUsed
                        })
                    } catch (error) {
                        return res.json({success: false, message: error})  
                    }
                default: return res.json({success: false, message: 'Not support network'})
            }
        }
    },
    createMetadata: async (req, res) => {
        const { name, image, description, animationUrl, courseId } = req.body;

        if(name === undefined || image === undefined || description === undefined) {
            return res.json({success: false, message: 'Name, Image, Description is require'})
        }
        try {
            const moralisModel = new MoralisModel()
            let metadata
            if(animationUrl) {
                metadata = {
                    name,
                    image,
                    description,
                    animation_url: animationUrl,
                    attributes: [
                        {
                            "trait_type": "Course ID", 
                            "value": courseId
                        }, 
                    ]
                };
            }
            else {
                metadata = {
                    name,
                    image,
                    description,
                    attributes: [
                        {
                            "trait_type": "Course ID", 
                            "value": courseId
                        }, 
                    ]
                };
            }
            const nftFileMetadataFile = new moralisModel.service.File('metadata.json', {
                base64: Buffer.from(JSON.stringify(metadata)).toString('base64'),
            });
            await nftFileMetadataFile.saveIPFS({ useMasterKey: true });
            const nftFileMetadataFilePath = nftFileMetadataFile.ipfs();

            return res.json({ 
                success: true, 
                message: 'Create Metadata', 
                metadata: nftFileMetadataFilePath 
            });
        } catch (err) {
            console.error(err);
            res.status(500).json({ success: false, message: 'Something went wrong', err });
        }
    },
    createNFT: async (req, res) => {
        const { 
            accountAddress, 
            privateKey, 
            metadata, 
            amount, 
            blockchain, 
            network, 
            lazyminting 
        } = req.body;
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }
        if(accountAddress === undefined || privateKey === undefined || metadata === undefined || amount === undefined) {
            return res.json({success: false, message: 'Account address, Private key, Metadata, Amount of NFT is required'})
        }
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})
        
        
        const web3Model = new Web3Model()

        let resNFT
        const createNFTMumbai = async () => {
            web3Model.web3Mumbai.eth.accounts.wallet.add(KEY_ADMIN);
            const contract = new web3Model.web3Mumbai.eth.Contract(
                POLYGON_ERC1155_ABI, NFT_MUMBAI_ADDRESS
            ) 
            const gasPrice = await contract.methods.mintToCaller(
                accountAddress, 
                amount, 
                metadata
            ).estimateGas({ from: ADMIN });

            const tx = await contract.methods.mintToCaller(
                accountAddress,
                amount, 
                metadata
            ).send({ from: ADMIN, gas: gasPrice, gasLimit: GAS_LIMIT});

            return tx
        }
        const createNFT = async (web3, tokenAddress, privateKey) => {
            web3.eth.accounts.wallet.add(privateKey);
            const contract = new web3.eth.Contract(ETH_BSC_ERC1155_ABI, tokenAddress)
            const gasPrice = await contract.methods.mint(
                accountAddress, 
                amount, 
                metadata
            ).estimateGas({ from: accountAddress });

            const tx = await contract.methods.mint(
                accountAddress,
                amount, 
                metadata
            ).send({ from: accountAddress, gas: gasPrice,  gasLimit: GAS_LIMIT});

            return tx
        }
        //Blockchain Ethereum
        if(blockchain === 'ETH') {
            if(network === 'rinkeby') {
                try {
                    resNFT = await createNFT(web3Model.web3ETH, NFT_RINKEBY_ADDRESS, privateKey)
                    return res.json({ 
                        success: true, 
                        message: `Create NFT`,
                        lazyminting: false,
                        token_address: NFT_RINKEBY_ADDRESS,
                        token_id: resNFT.events.TransferSingle.returnValues.id,
                        transaction_hash: resNFT.transactionHash,
                        blockchain,
                        network
                    });
                } catch (err) {
                    console.error('Lỗi rồi', err);
                    return res.status(500).json({ 
                        success: false, 
                        message: `Some thing went wrong`, err 
                    })
                }
            }
        }
        //Blockchain Binance
        if(blockchain === 'BSC') {
            if (network === 'bsc testnet') {
                try {
                    resNFT = await createNFT(web3Model.web3Bsc, NFT_BSC_TESTNET_ADDRESS, privateKey)
                    res.json({ 
                        success: true, 
                        message: `Create NFT`, 
                        lazyminting: false,
                        token_address: NFT_BSC_TESTNET_ADDRESS,
                        token_id: resNFT.events.TransferSingle.returnValues.id, 
                        transaction_hash: resNFT.transactionHash,
                        gas_used: resNFT.gasUsed,
                        blockchain,
                        network
                    });
                } catch (err) {
                    console.error('Lỗi rồi', err);
                    return res.status(500).json({ 
                        success: false, 
                        message: `Some thing went wrong`, err 
                    })
                }
            }
        }
        //Lazy minting in Mumbai
        if(lazyminting) {
            try {
                resNFT = await createNFTMumbai()
                return res.json({ 
                    success: true, 
                    message: `Create NFT`, 
                    lazyminting: true,
                    token_address: NFT_MUMBAI_ADDRESS,
                    token_id: resNFT.events.TransferSingle.returnValues.id, 
                    transaction_hash: resNFT.transactionHash,
                    blockchain,
                    network
                });
            } catch (err) {
                console.error('Lỗi rồi', err.message);
                return res.status(500).json({ 
                    success: false, 
                    message: `Some thing went wrong`, err 
                })
            }
        }
    }
}

module.exports = NFTController