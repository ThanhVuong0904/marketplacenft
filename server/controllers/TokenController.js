const Web3Model = require('../models/Web3Model')
const {SUPPORT_NETWORK, SUPPORT_BLOCKCHAIN} = require('../constants')
const TOKEN_ABI = require('../abi/Token')

const TokenController = {
    transfer: async (req, res) => {
        const {tokenAddress, from, to, amount, blockchain, network} = req.body
        
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }

        if(tokenAddress === undefined) {
            return res.json({success: false, message: 'Token Address is required'})
        }

        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})
        
        const web3Model = new Web3Model()
        const checkBalance = async (web3, tokenAddress) => {
            const contract  = new web3.eth.Contract(TOKEN_ABI, tokenAddress);
            const balance = await contract.methods.balanceOf(from.accountAddress).call()
            return balance
        }
        const transferToken = async (web3, tokenAddress) => {
            web3.eth.accounts.wallet.add(from.privateKey);
            const contract  = new web3.eth.Contract(TOKEN_ABI, tokenAddress);
            const gasEstimated = await contract.methods.transfer(
                to, 
                amount
            ).estimateGas({ from: from.accountAddress });
            console.log(Math.ceil(gasEstimated, 1.1))

            const tx = await contract.methods.transfer(
                to, 
                amount
            ).send({ from: from.accountAddress, gasLimit: Math.ceil(gasEstimated, 1.1) });

            return tx
        }

        if(blockchain === "ETH") {
            switch(network) {
                case 'rinkeby':
                    try {
                        const balance = await checkBalance(web3Model.web3ETH, tokenAddress)
                        console.log(balance);
                        if(balance < amount) {
                            return res.json({
                                success: false, 
                                message: 'ERC20: transfer amount exceeds balance'
                            })
                        }
                        let tx = await transferToken(web3Model.web3ETH, tokenAddress)
                        
                        return res.json({
                            success: true, message: `Transfer Token`,
                            token_address: tokenAddress,
                            from: from.accountAddress,
                            to,
                            amount,
                            blockchain,
                            type: 'ERC 20',
                            testnet: network,
                            transaction_hash: tx.transactionHash,
                            gas_used: tx.gasUsed
                        })
                    } catch (error) {
                        console.log(error)
                        return res.json({success: false, message: error})
                    }
                default: return res.json({success: false, message: 'Not support network'})
            }
        }
        if(blockchain === "BSC") {
            switch(network) {
                case 'bsc testnet':
                    try {
                        const balance = await checkBalance(web3Model.web3Bsc, tokenAddress)
                        console.log(balance);
                        if(balance < amount) {
                            return res.json({
                                success: false, 
                                message: 'BEP20: transfer amount exceeds balance'
                            })
                        }
                        let tx = await transferToken(web3Model.web3Bsc, tokenAddress)
                        
                        return res.json({
                            success: true, message: `Transfer Token`,
                            token_address: tokenAddress,
                            from: from.accountAddress,
                            to,
                            amount,
                            blockchain,
                            type: 'BEP 20',
                            testnet: network,
                            transaction_hash: tx.transactionHash,
                            gas_used: tx.gasUsed
                        })
                    } catch (error) {
                        console.log(error);
                        return res.json({success: false, message: error})  
                    }
                default: return res.json({success: false, message: 'Not support network'})
            }
        }
    }
}

module.exports = TokenController