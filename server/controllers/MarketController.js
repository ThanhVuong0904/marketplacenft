const MARKET_ABI = require('../abi/Market');
const { ETH_BSC_ERC1155_ABI } = require('../abi/NFT');
const { MARKET_RINKEBY_ADDRESS, MARKET_BSC_TESTNET_ADDRESS, SUPPORT_BLOCKCHAIN, SUPPORT_NETWORK } = require('../constants')
const Web3Model = require('../models/Web3Model')

const GAS_LIMIT = '10000000000'

const MarketController = {
    getAlls: async (req, res) => {

        const web3Model = new Web3Model()
        const contract = new web3Model.web3ETH.eth.Contract(MARKET_ABI, MARKET_RINKEBY_ADDRESS)
        let txRinkeby = await contract.methods.getItems().call()
        let itemRinkeby = txRinkeby.map(i => {
            return {
                tokenId: i.tokenId,
                tokenAddress: i.tokenAddress,
                seller: i.seller,
                askingPrice: i.askingPrice,
                amount: i.amount,
                isSold: i.isSold,
            }
        })

        const contractBSC = new web3Model.web3Bsc.eth.Contract(MARKET_ABI, MARKET_BSC_TESTNET_ADDRESS)


        let txBsc = await contractBSC.methods.getItems().call()
        let itemBsc = txBsc.map(i => {
            return {
                tokenId: i.tokenId,
                tokenAddress: i.tokenAddress,
                seller: i.seller,
                askingPrice: i.askingPrice,
                amount: i.amount,
                isSold: i.isSold,
            }
        })

        return res.json({
            success: true, 
            message: 'Get all NFT in market place', 
            // marketPlaceRinkeby: itemRinkeby,
            marketPlaceBsc: itemBsc,
        })
    },
    add: async (req, res) => {
        const {
            accountAddress, 
            privateKey,
            tokenAddress,
            tokenId,
            amount, 
            askingPrice,
            blockchain,
            network
        } = req.body
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})
        const web3Model = new Web3Model()

        const checkIsApproveForAll = async (web3, tokenAddress, marketAddress) => {
            //ETH_BSC_ERC1155_ABI vs POLYGON_ERC1155_ABI chỉ khác nhau mỗi hàm mint
            //Đây là hàm check xem đã approve chưa nên sài ABI cũng dc
            const contractNFT = new web3.eth.Contract(ETH_BSC_ERC1155_ABI, tokenAddress)
            const isApproveForAll = await contractNFT.methods.isApprovedForAll(
                accountAddress, marketAddress
            ).call({ from: accountAddress })
            console.log("isApproveForAll", isApproveForAll)
            return isApproveForAll
        }

        const setApprovalForAll = async (web3, privateKey,tokenAddress, marketAddress) => {
            web3.eth.accounts.wallet.add(privateKey);
            const contractNFT = new web3.eth.Contract(ETH_BSC_ERC1155_ABI, tokenAddress)

            const gasPrice = await contractNFT.methods.setApprovalForAll(
                marketAddress, true
            ).estimateGas({ from: accountAddress })

            await contractNFT.methods.setApprovalForAll(marketAddress, true).send({
                from: accountAddress, gas: gasPrice, gasLimit: GAS_LIMIT
            })
        }

        const addMarket = async (web3, privateKey, marketAddress) => {
            web3.eth.accounts.wallet.add(privateKey);
            const contract = new web3.eth.Contract(MARKET_ABI, marketAddress)

            const gasPrice = await contract.methods.addItemToMarket(
                tokenId, 
                web3.utils.toWei(askingPrice, 'ether'),
                amount, 
                tokenAddress,
            ).estimateGas({ from: accountAddress })

            const tx = await contract.methods.addItemToMarket(
                tokenId, 
                web3.utils.toWei(askingPrice, 'ether'),
                amount, 
                tokenAddress,
            )
            .send({from: accountAddress, gas: gasPrice, gasLimit: GAS_LIMIT})
            .catch(error => {
                console.log(error);
                return res.json({success: false, message: error.reason})
            })

            return tx
        }
        
        if(blockchain === 'ETH') {
            if(network === 'rinkeby') {
                try {
                    const isApproveForAll = await checkIsApproveForAll(
                        web3Model.web3ETH, tokenAddress, MARKET_RINKEBY_ADDRESS
                    );
                    if(!isApproveForAll) {
                        await setApprovalForAll(
                            web3Model.web3ETH, privateKey, tokenAddress, MARKET_RINKEBY_ADDRESS
                        )
                    }
                    const tx = await addMarket(
                        web3Model.web3ETH, privateKey, MARKET_RINKEBY_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Add NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }

        if(blockchain === 'BSC') {
            if(network === 'bsc testnet') {
                try {
                    const isApproveForAll = await checkIsApproveForAll(
                        web3Model.web3Bsc, tokenAddress, MARKET_BSC_TESTNET_ADDRESS
                    );
                    if(!isApproveForAll) {
                        await setApprovalForAll(
                            web3Model.web3Bsc, privateKey, tokenAddress, MARKET_BSC_TESTNET_ADDRESS
                        )
                    }
                    const tx = await addMarket(
                        web3Model.web3Bsc, privateKey, MARKET_BSC_TESTNET_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Add NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }
    },
    buy: async (req, res) => {
        const {
            accountAddress, 
            privateKey,
            tokenAddress,
            tokenId,
            price,
            blockchain,
            network
        } = req.body
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})

        const web3Model = new Web3Model()

        
        const buyItem = async (web3, privateKey, marketAddress) => {
            web3.eth.accounts.wallet.add(privateKey);
            const contract = new web3.eth.Contract(MARKET_ABI, marketAddress)

            const gasPrice = await contract.methods.buyItem(
                tokenAddress,
                tokenId, 
            ).estimateGas({ from: accountAddress, value: web3.utils.toWei(price, "ether")})

            const tx = await contract.methods.buyItem(
                tokenAddress,
                tokenId, 
            )
            .send({
                from: accountAddress, 
                gas: gasPrice, 
                gasLimit: GAS_LIMIT, 
                value: web3.utils.toWei(price, "ether")
            })
            .catch(error => {
                console.log(error);
                return res.json({success: false, message: error.reason})
            })

            return tx
        }
        
        if(blockchain === 'ETH') {
            if(network === 'rinkeby') {
                try {
                    
                    const tx = await buyItem(
                        web3Model.web3ETH, privateKey, MARKET_RINKEBY_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Buy NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        price,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }

        if(blockchain === 'BSC') {
            if(network === 'bsc testnet') {
                try {
                    const tx = await buyItem(
                        web3Model.web3Bsc, privateKey, MARKET_BSC_TESTNET_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Buy NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        price,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }
    },
    cancel: async (req, res) => {
        const {
            accountAddress, 
            privateKey,
            tokenAddress,
            tokenId,
            blockchain,
            network
        } = req.body
        if(blockchain === undefined || network === undefined) {
            return res.json({success: false, message: 'Blockchain and Network is required'})
        }
        if(!SUPPORT_BLOCKCHAIN.includes(blockchain)) 
            return res.json({success: false, message: `Only the following blockchains are supported ${[...SUPPORT_BLOCKCHAIN]}`})
        
        if(!SUPPORT_NETWORK.includes(network)) 
            return res.json({success: false, message: `Only the following network are supported ${[...SUPPORT_NETWORK]}`})

        const web3Model = new Web3Model()

        
        const cancelItem = async (web3, privateKey, marketAddress) => {
            web3.eth.accounts.wallet.add(privateKey);
            const contract = new web3.eth.Contract(MARKET_ABI, marketAddress)

            const gasPrice = await contract.methods.cancel(
                tokenAddress,
                tokenId, 
            ).estimateGas({ from: accountAddress})

            const tx = await contract.methods.cancel(
                tokenAddress,
                tokenId, 
            )
            .send({
                from: accountAddress, 
                gas: gasPrice, 
                gasLimit: GAS_LIMIT, 
            })
            .catch(error => {
                console.log(error);
                return res.json({success: false, message: error.reason})
            })

            return tx
        }
        
        if(blockchain === 'ETH') {
            if(network === 'rinkeby') {
                try {
                    
                    const tx = await cancelItem(
                        web3Model.web3ETH, privateKey, MARKET_RINKEBY_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Cancel NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }

        if(blockchain === 'BSC') {
            if(network === 'bsc testnet') {
                try {
                    const tx = await cancelItem(
                        web3Model.web3Bsc, privateKey, MARKET_BSC_TESTNET_ADDRESS
                    )
                    return res.json({
                        success: true, 
                        message: `Cancel NFT`, 
                        token_address: tokenAddress,
                        token_id: tokenId,
                        blockchain,
                        network,
                        transaction_hash: tx.transactionHash,
                        gas_used: tx.gasUsed
                    })
                } 
                catch (err) {
                    console.log(err);
                    return res.status(500).json({ 
                        success: false, 
                        message: 'Something went wrong', err 
                    });
                }
            }
        }
    }
}

module.exports = MarketController