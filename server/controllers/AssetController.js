const cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET,
});

const AssetController = {
    upload: async (req, res) => {
        const { asset } = req.files;
        console.log(asset);
        const isVideo = asset.mimetype.includes('video')
        console.log(isVideo);
        try {
            if(isVideo) {
                if(asset.size > 104857600) {
                    return res.json({success: false, err: 'Size to large'})
                }
                await cloudinary.v2.uploader.upload_large(
                    asset.tempFilePath, 
                    { resource_type: "video" },
                    function(err, result) {
                        if(err) {
                            res.json({ success: false, err }); 
                        }
                        else {
                            res.json({ success: true, message: `Upload asset ${asset.mimetype} to Cloudinary`, url: result.secure_url });
                        }
                    }
                );
            }
            else {
                const response = await cloudinary.v2.uploader.upload(
                    asset.tempFilePath,
                    { folder: 'upload_image' },
                    async (err, result) => {
                        if (err) throw err;
                        return result.secure_url;
                    },
                );
                res.json({ success: true, message: `Upload asset ${asset.mimetype} to Cloudinary`, url: response.secure_url });
            }
        } catch (err) {
            console.error(err);
            return res.status(500).json({ success: false, message: 'Something went wrong', err });
        }
    }
}

module.exports = AssetController