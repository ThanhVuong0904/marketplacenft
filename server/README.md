# MarketPlaceNFT

# Run Server

cd đến folder server
1. `npm install`
2. `npm run dev`



# API Docs

### Link Post man
`https://www.getpostman.com/collections/96fc109162f852ba967f`

# Account
### Create Wallet
**[GET]** `http://localhost:8000/account/create-wallet`
-Tạo ví cho user

Return value
```js
{
    "success": true,
    "message": "Create wallet",
    "wallet": {
        "address": "0xf005c8f81AE8c65d8acbfB4fA1Eec6F629457829",
        "private_key": "0x171c117bd788f94fabec6f4ea7170411a9f49a9abc129d3f69447fbceacba66a",
        "mnemonic": "barely identify park claw ribbon lesson only worry trick test ride ostrich"
    }
}
```

### Get balance navtive of account
**[GET]** `http://localhost:8000/account/native/:accountAddress?blockchain?network`

**blockchain:** ETH, BSC, Polygon
**network:** rinkeby, bsc testnet, mumbai

Return value

```js
{
    "success": true,
    "message": "Get your native balance",
    "balance": "285582353813178889",
    "address": "0x6Ae939e3695e8F936F90Fd7a44e3C4D928FB4407",
    "blockchain": "ETH",
    "network": "rinkeby"
}
```

### Get All NFT of account
**[GET]** `http://localhost:8000/account/:accountAddress/nft/:nftAddress?blockchain?network`

**blockchain:** ETH, BSC, Polygon
**network:** rinkeby, bsc testnet, mumbai

Return value

```js
{
    "success": true,
    "message": "Get NFTs owner",
    "nfts": [
        {
            "token_address": "0xb592a499c2bd9c57761078a48e6e5af71b68bde2",
            "token_id": "12",
            "owner_of": "0x6ae939e3695e8f936f90fd7a44e3c4d928fb4407",
            "block_number": "10975261",
            "block_number_minted": "10975261",
            "token_hash": "232bdc185482e299610b7f4f6a19afda",
            "amount": "75",
            "contract_type": "ERC1155",
            "name": "AirClass",
            "symbol": "AIR",
            "token_uri": "https://ipfs.moralis.io:2053/ipfs/QmRxdwzL3rzAEzJyA1GQ47f2FJBGLFuEJDexyAcrSgJVv4",
            "metadata": "{\"name\":\"Vuong test\",\"image\":\"https://res.cloudinary.com/dcahbrrcb/image/upload/v1651927457/upload_image/qyljhfd3nra1kozphmkh.jpg\",\"description\":\"Mota\",\"attributes\":[{\"trait_type\":\"Course ID\",\"value\":\"123\"}]}",
            "last_token_uri_sync": "2022-07-06T05:42:40.185Z",
            "last_metadata_sync": "2022-07-06T05:42:47.490Z"
        }
    ],
    "blockchain": "ETH",
    "network": "rinkeby"
}
```

# Asset
### Upload Asset to Cloudinary

**[POST]** `http://localhost:8000/asset/upload`

![image](https://user-images.githubusercontent.com/68543789/167311634-662784dc-280f-4037-b6d5-fc5330c5a0a2.png)

Return value
```js
{
    "success": true,
    "message": "Upload asset image/jpeg to Cloudinary",
    "url": "https://res.cloudinary.com/dcahbrrcb/image/upload/v1652029336/upload_image/so9wf975xjf3gznasfc0.jpg"
}
```

# Token

### Transfer Token
**[POST]** `http://localhost:8000/token/transfer`

![image](https://user-images.githubusercontent.com/68543789/177950419-ed8935e4-b2cb-4299-93c5-33095fa228c2.png)


**Params:**
- **tokenId**: id của NFT
- **tokenAddress**: address của NFT
- **from**: `object` gồm accountAddress và privateKey của người gửi
- **to**: account address cùa người nhận
- **nftType**: ERC1155, ERC721
- **amount**: Số lượng muốn chuyển
- **blockchain**: ETH, BSC, Polygon
- **network**: rinkeby, bsc testnet, mumbai



# NFT

### Create Metadata
**[POST]** `http://localhost:8000/nft/create-metadata`

**Params:**
- **name**: Tên NFT
- **image**: Hình ảnh NFT
- **description**: Mô tả NFT
- **animationUrl**: Link Video NFT
- **courseId**: ID Khóa học 

Return values
```js
{
    "success": true,
    "message": "Create Metadata",
    "metadata": "https://ipfs.moralis.io:2053/ipfs/QmRxdwzL3rzAEzJyA1GQ47f2FJBGLFuEJDexyAcrSgJVv4"
}
```

### Create NFT
**[POST]** `https://localhost:8000/nft/mint`

**Params:**
- **accountAddress**: Địa chỉ ví muốn tạo NFT
- **privateKey**: Khóa riêng tư của account
- **metadata**: Metadata NFT (uri)
- **amount**: Số lượng NFT cần tạo 
- **blockchain**: ETH, BSC, Polygon
- **network**: rinkeby, bsc testnet, mumbai
- **lazyminting**: default `false`, nếu `true` sẽ mint ở Polygon Mumbai và Admin sẽ chịu phí mint

![image](https://user-images.githubusercontent.com/68543789/177936921-8f979117-e012-426f-bca7-a34f1d240146.png)


Return value

```js
{
    "success": true,
    "message": "Create NFT",
    "lazyminting": false,
    "token_address": "0x0B8495F5D3Aa177c5cdf50CA1a8Ad6411C0f95bA",
    "token_id": "12",
    "transaction_hash": "0x463816f9ac10fbcafb697c5b370dbf3c0de1070e9aca4e4a32dd27008ec7a990",
    "gas_used": 139513,
    "blockchain": "BSC",
    "network": "bsc testnet"
}
```

### Transfer NFT

**[POST]** `https://localhost:8000/nft/transfer`

![image](https://user-images.githubusercontent.com/68543789/177938934-60483ac4-cd6c-4618-829c-df67713199cb.png)


**Params:**
- **tokenId**: id của NFT
- **tokenAddress**: address của NFT
- **from**: `object` gồm accountAddress và privateKey của người gửi
- **to**: account address cùa người nhận
- **nftType**: ERC1155, ERC721
- **amount**: Số lượng muốn chuyển
- **blockchain**: ETH, BSC, Polygon
- **network**: rinkeby, bsc testnet, mumbai

Return values

```js
{
    "success": true,
    "message": "Transfer NFT",
    "token_id": 12,
    "token_address": "0xb592a499c2bd9c57761078a48e6e5af71b68bde2",
    "nft_type": "ERC1155",
    "from": "0x6Ae939e3695e8F936F90Fd7a44e3C4D928FB4407",
    "to": "0x83D36D4310672484eBCAA3c727B0ef5A428E0524",
    "amount": 5,
    "blockchain": "ETH",
    "testnet": "rinkeby",
    "transaction_hash": "0xf760a8cb5e3e53ad72fe7a0f43a67bb49c7f35769ec7f3a27b56b4a1777a5785",
    "gas_used": 41287
}
```


# Market

### Add NFT to Market

**[POST]** `http://localhost:8000/market/add`

![image](https://user-images.githubusercontent.com/68543789/177939679-220a8516-559e-4126-972b-887cd81a1c94.png)

**Params:**
- **accountAddress**: Địa chỉ ví muốn Add vào market
- **privateKey**: Khóa riêng tư của account
- **tokenAddress**: Address của NFT
- **tokenId**: ID của NFT
- **amount**: Số lượng NFT muốn bán
- **askingPrice**: Giá bán (Ether)
- **blockchain**: ETH, BSC
- **network**: rinkeby, bsc testnet



Return value

```js
{
    "success": true,
    "message": "Add NFT",
    "token_address": "0x0B8495F5D3Aa177c5cdf50CA1a8Ad6411C0f95bA",
    "token_id": "10",
    "blockchain": "BSC",
    "network": "bsc testnet",
    "transaction_hash": "0x4b5523a1d7561c898677fd896c83c43c99278a85dff2720852a20ccb230ad4b1",
    "gas_used": 45222
}
```



### Get NFT in Market

**[GET]** `https://localhost:8080/market/all-nft`

Return value

```js
{
    "success": true,
    "message": "Get all NFT in market place",
    "marketPlaceRinkeby": [
        {
            "tokenId": "2",
            "tokenAddress": "0xb592a499c2bd9C57761078A48e6E5af71B68BdE2",
            "seller": "0x858077C61c11b9e3e01807B35d705E610bae64Ed",
            "askingPrice": "5000000000000000",
            "amount": "10",
            "isSold": false
        }
    ],
    "marketPlaceBsc": [
        {
            "tokenId": "2",
            "tokenAddress": "0x0B8495F5D3Aa177c5cdf50CA1a8Ad6411C0f95bA",
            "seller": "0x20b8D104081D512421040b1048f7512608Ea2f26",
            "askingPrice": "6000000000000000",
            "amount": "10",
            "isSold": true
        }
    ]
}
```

### Buy NFT
**[POST]** `http://localhost:8000/market/buy`

![image](https://user-images.githubusercontent.com/68543789/177942172-5d97f4e5-6bf4-4948-8f3c-55cff1bcedd1.png)

**Params:**
- **accountAddress**: Địa chỉ ví người mua
- **privateKey**: Khóa riêng tư của người mua
- **tokenAddress**: Address của NFT
- **tokenId**: ID của NFT
- **amount**: Số lượng NFT mua
- **price**: Giá bán (Ether)
- **blockchain**: ETH, BSC
- **network**: rinkeby, bsc testnet



Return value

```js
{
    "success": true,
    "message": "Buy NFT",
    "token_address": "0x0B8495F5D3Aa177c5cdf50CA1a8Ad6411C0f95bA",
    "token_id": "10",
    "price": "0.006",
    "blockchain": "BSC",
    "network": "bsc testnet",
    "transaction_hash": "0x72d5c78a0ad2b48c49b4b29f90989f21361cdd65b672b54882c24d48969797da",
    "gas_used": 100837
}
```


### Cancel sell NFT

**[POST]** `https://localhost:8000/market/cancel`

![image](https://user-images.githubusercontent.com/68543789/177946966-f10be469-bd19-40c6-8c6e-2126554de1d2.png)

**Params:**
- **accountAddress**: Địa chỉ ví người hủy bán
- **privateKey**: Khóa riêng tư của người hủy bán
- **tokenAddress**: Address của NFT
- **tokenId**: ID của NFT
- **blockchain**: ETH, BSC
- **network**: rinkeby, bsc testnet



Return value

```js
{
    "success": true,
    "message": "Cancel NFT",
    "token_address": "0x0B8495F5D3Aa177c5cdf50CA1a8Ad6411C0f95bA",
    "token_id": "10",
    "blockchain": "BSC",
    "network": "bsc testnet",
    "transaction_hash": "0xf4e78d90b22fcbe7db54c72aa9cfba80bbfddc01217816dcb5beb6599dd2600f",
    "gas_used": 37434
}
```
