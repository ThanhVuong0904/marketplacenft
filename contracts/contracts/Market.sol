// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
// import "@openzeppelin/contracts/token/ERC721/ERC721.sol";


contract Marketplace {
    struct Item {
        uint256 tokenId;
        uint256 askingPrice;
        uint256 amount;
        address tokenAddress;
        address payable seller;
        bool isSold;
    }

    Item [] public items;
    mapping (uint256 => Item) public _item;
    mapping (address => mapping(uint256 => bool)) public _nftInitAdd;
    mapping (address => mapping(uint256 => uint256)) public _listingId;
    uint256 public _marketId = 0;

    event ItemAdded(uint256 listingId, uint256 tokenId, uint256 askingPrice, uint256 amount, address tokenAddress, address seller);
    event ItemSold(uint256 listingId, uint256 tokenId, uint256 amount, address buyer, address seller);
    event Cancel(uint256 listingId, uint256 tokenId);

    modifier HasEnoughQuantity(address tokenAddress, uint256 tokenId, uint256 amount){
        IERC1155 tokenContract = IERC1155(tokenAddress);
        require(tokenContract.balanceOf(msg.sender, tokenId) >= amount, "AIR CLASS: AIR CLASS: Not enough quantity");
        _;
    }

    modifier IsForSale(address tokenAddress, uint256 tokenId){
        require(_item[_listingId[tokenAddress][tokenId]].isSold == true, "AIR CLASS: Item is not already sold!");
        _;
    }
    modifier IsExist(address tokenAddress, uint256 tokenId){
        require(_item[_listingId[tokenAddress][tokenId]].tokenAddress == tokenAddress && _item[_listingId[tokenAddress][tokenId]].tokenId == tokenId, "AIR CLASS: Item not exist");
        _;
    }

    modifier HasTransferApproval(address tokenAddress, address owner){
        IERC1155 tokenContract = IERC1155(tokenAddress);
        require(tokenContract.isApprovedForAll(owner, address(this)) == true, "AIR CLASS: Item has not been approved");
        _;
    }

    function addItemToMarket(
        uint256 tokenId, 
        uint256 askingPrice,
        uint256 amount,
        address tokenAddress
    ) HasEnoughQuantity(tokenAddress, tokenId, amount) 
    HasTransferApproval (tokenAddress, msg.sender) external {
        if(_nftInitAdd[tokenAddress][tokenId] == false) {
            // _item[_marketId] = Item(tokenId, tokenAddress, payable(msg.sender), askingPrice, true);
            items.push(_item[_marketId] = Item(tokenId, askingPrice, amount, tokenAddress, payable(msg.sender), true));
            _nftInitAdd[tokenAddress][tokenId] = true;
            _listingId[tokenAddress][tokenId] = _marketId;
            _marketId++;
        }
        else {
            _item[_listingId[tokenAddress][tokenId]].seller = payable(msg.sender);
            _item[_listingId[tokenAddress][tokenId]].askingPrice = askingPrice;
            _item[_listingId[tokenAddress][tokenId]].amount = amount;
            _item[_listingId[tokenAddress][tokenId]].isSold = true;
        }
        emit ItemAdded(_marketId, tokenId, askingPrice, amount, tokenAddress, msg.sender);
    }

    function buyItem(address tokenAddress, uint256 tokenId) payable external IsForSale(tokenAddress, tokenId) IsExist(tokenAddress, tokenId) {
        require(msg.value >= _item[_listingId[tokenAddress][tokenId]].askingPrice, "AIR CLASS: Not enough funds sent");
        require(msg.sender != _item[_listingId[tokenAddress][tokenId]].seller, "AIR CLASS: You cannot buy your own");
        

        IERC1155(_item[_listingId[tokenAddress][tokenId]].tokenAddress)
        .safeTransferFrom(
            _item[_listingId[tokenAddress][tokenId]].seller, 
            msg.sender,
            _item[_listingId[tokenAddress][tokenId]].tokenId,
            _item[_listingId[tokenAddress][tokenId]].amount,
            ""
        );
        
        _item[_listingId[tokenAddress][tokenId]].seller.transfer(msg.value);
        _item[_listingId[tokenAddress][tokenId]].seller = payable(msg.sender);
        _item[_listingId[tokenAddress][tokenId]].isSold = false;

        emit ItemSold(
            _marketId, 
            _item[_listingId[tokenAddress][tokenId]].tokenId, 
            _item[_listingId[tokenAddress][tokenId]].amount,
            msg.sender, 
            _item[_listingId[tokenAddress][tokenId]].seller
        );
    }

    function cancel(address tokenAddress, uint256 tokenId) external {
        require(_item[_listingId[tokenAddress][tokenId]].seller == msg.sender, "AIR CLASS: You don't have access");
        require(_item[_listingId[tokenAddress][tokenId]].isSold == true, "AIR CLASS: NFT don't sell");
        _item[_listingId[tokenAddress][tokenId]].isSold = false;
        emit Cancel(_listingId[tokenAddress][tokenId], _item[_listingId[tokenAddress][tokenId]].tokenId);
    }

    function getItems() public view returns (Item[] memory){
        Item[] memory _items = new Item[](_marketId);
        for (uint i = 0; i < _marketId; i++) {
            Item storage listItem = _item[i];
            _items[i] = listItem;
        }
        return _items;
    }
}