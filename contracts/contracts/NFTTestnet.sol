// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";


contract AirClassTestNet is ERC1155, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdCounter;
    constructor() ERC1155("") {}
    mapping (uint256 => string) private _uris;

    string public name = "AirClass";
    string public symbol = "AIR";

    function mintToCaller(address caller, uint256 amount, string memory _uri)
        public
        onlyOwner
    {

        _tokenIdCounter.increment();
        uint256 tokenId = _tokenIdCounter.current();
        _mint(caller, tokenId, amount, "");
        setTokenUri(tokenId, _uri);
    }

    function increment (address account, uint256 id, uint256 amount) public onlyOwner {
        require(_ownerOf(account, id) == true, "AIR CLASS: You do not own this NFT");
        _mint(account, id, amount, "");
    }
    

    function uri(uint256 tokenId) override public view returns (string memory) {
        return(_uris[tokenId]);
    }
    
    function setTokenUri(uint256 tokenId, string memory _uri) private {
        require(bytes(_uris[tokenId]).length == 0, "Cannot set uri twice"); 
        _uris[tokenId] = _uri; 
    }

    function _ownerOf(address account, uint256 tokenId) internal view returns (bool) {
        return balanceOf(account, tokenId) != 0;
    }
}
