// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/utils/Counters.sol";


contract AirClass is ERC1155 {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIdCounter;
    constructor() ERC1155("") {}
    mapping (uint256 => string) private _uris;

    string public name = "AirClass";
    string public symbol = "AIR";

    function mint(address account, uint256 amount, string memory _uri)
        public
    {

        _tokenIdCounter.increment();
        uint256 tokenId = _tokenIdCounter.current();
        _mint(account, tokenId, amount, "");
        setTokenUri(tokenId, _uri);
    }

    function mintFromTestnet (
        address from, 
        address to, 
        uint256 amount, 
        uint256 askingPrice,
        string memory _uri
    ) payable public {
        require(msg.value == askingPrice, "AIR CLASS: Not enough funds sent");
        _tokenIdCounter.increment();
        uint256 tokenId = _tokenIdCounter.current();
        payable(from).transfer(msg.value);
        _mint(to, tokenId, amount, "");
        setTokenUri(tokenId, _uri);
    }

    function increment (address account, uint256 id, uint256 amount) public {
        require(_ownerOf(account, id) == true, "AIR CLASS: You do not own this NFT");
        _mint(account, id, amount, "");
    }


    function uri(uint256 tokenId) override public view returns (string memory) {
        return(_uris[tokenId]);
    }
    
    function setTokenUri(uint256 tokenId, string memory _uri) private {
        require(bytes(_uris[tokenId]).length == 0, "Cannot set uri twice"); 
        _uris[tokenId] = _uri; 
    }

    function _ownerOf(address account, uint256 tokenId) internal view returns (bool) {
        return balanceOf(account, tokenId) != 0;
    }
}