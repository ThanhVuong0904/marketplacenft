const { expect } = require("chai");
const { ethers } = require("hardhat");

describe.only("NFT 1155 Testnet", function () {
    let deployer, user, user2;
    beforeEach(async function () {
        [deployer, user, user2] = await ethers.getSigners();
        const NFT = await ethers.getContractFactory("AirClassTestNet", deployer);
        this.nft = await NFT.deploy();
    });

    it("Admin Có thể mint NFT và set URI", async function () {
        await this.nft.connect(deployer).mintToCaller(user.address, 10, "uri1");
        expect(await this.nft.uri(1)).to.eq("uri1");
        expect(await this.nft.balanceOf(user.address, 1)).to.eq(10);
    });

    it("Admin Có thể increment NFT", async function () {
        await this.nft.connect(deployer).mintToCaller(user.address, 10, "uri1");
        expect(await this.nft.uri(1)).to.eq("uri1");
        expect(await this.nft.balanceOf(user.address, 1)).to.eq(10);

        await this.nft.connect(deployer).increment(user.address, 1, 10);
        expect(await this.nft.balanceOf(user.address, 1)).to.eq(20);
    });

    it("Ngoài Admin không ai có thể mint NFT và setURI", async function () {
        await expect(this.nft.connect(user).mintToCaller(user.address, 10, "uri1")).to.revertedWith(
            "Ownable: caller is not the owner"
        );
    });

    it("Không thể increment NFT khi chưa tạo NFT", async function () {
        await expect(this.nft.connect(deployer).increment(user.address, 1, 10)).to.revertedWith(
            "AIR CLASS: You do not own this NFT"
        );
    });
});
