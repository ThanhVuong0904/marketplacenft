const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Market", function () {
    let deployer, user, user2;
    beforeEach(async function () {
        [deployer, user, user2] = await ethers.getSigners();
        const Market = await ethers.getContractFactory("Marketplace", deployer);
        this.market = await Market.deploy();
        const NFT = await ethers.getContractFactory("AirClass", deployer);
        this.nft = await NFT.deploy();
    });

    it("Không thể Add vượt quá số lượng NFT hiện có của người ADD", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        // await this.market.connect(user).addItemToMarket(1, eth, 15, this.nft.address);
        await expect(this.market.connect(user).addItemToMarket(1, eth, 15, this.nft.address)).to.revertedWith(
            "AIR CLASS: AIR CLASS: Not enough quantity"
        );
    });

    it("Không thể Add khi người ADD chưa approval cho Market", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await expect(this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address)).to.revertedWith(
            "AIR CLASS: Item has not been approved"
        );
    });

    it("Người sở hữu NFT Có thể Add NFT vào market", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);
        const allMarket = await this.market.getItems();
        expect(allMarket[0].tokenId).to.eq(1);
        expect(allMarket[0].askingPrice.toString()).to.eq(eth);
        expect(allMarket[0].amount).to.eq(5);
        expect(allMarket[0].tokenAddress).to.eq(this.nft.address);
        expect(allMarket[0].seller).to.eq(user.address);
    });

    it("Không thể Buy NFT do mình rao bán", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await expect(this.market.connect(user).buyItem(this.nft.address, 1, { value: eth })).to.revertedWith(
            "AIR CLASS: You cannot buy your own"
        );
    });

    it("Không thể Buy NFT khi thiếu tiền", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await expect(this.market.connect(user).buyItem(this.nft.address, 1)).to.revertedWith(
            "AIR CLASS: Not enough funds sent"
        );
    });

    it("Không thể Buy NFT khi NFT không có trong market", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await expect(this.market.connect(user2).buyItem(this.nft.address, 2, { value: eth })).to.revertedWith(
            "AIR CLASS: Item not exist"
        );
    });

    it("Có thể Buy NFT", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await this.market.connect(user2).buyItem(this.nft.address, 1, { value: eth });

        const balanceNFTUser1Af = await this.nft.balanceOf(user.address, 1);
        const balanceNFTUser2Af = await this.nft.balanceOf(user2.address, 1);

        expect(balanceNFTUser1Af).to.eq(5);
        expect(balanceNFTUser2Af).to.eq(5);
    });

    it("Không thể hủy rao bán NFT khi không phải là chủ sở hữu NFT", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await expect(this.market.connect(user2).cancel(this.nft.address, 1)).to.revertedWith(
            "AIR CLASS: You don't have access"
        );
    });

    it("Không thể hủy rao bán NFT khi NFT đang không rao bán", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);
        await this.market.connect(user).cancel(this.nft.address, 1);

        await expect(this.market.connect(user).cancel(this.nft.address, 1)).to.revertedWith(
            "AIR CLASS: NFT don't sell"
        );
    });

    it("Có thể hủy rao bán NFT", async function () {
        const eth = ethers.utils.parseEther("100");
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).setApprovalForAll(this.market.address, true);
        await this.market.connect(user).addItemToMarket(1, eth, 5, this.nft.address);

        await this.market.connect(user).cancel(this.nft.address, 1);

        const allMarket = await this.market.getItems();
        expect(allMarket[0].isSold).to.eq(false);
        expect(allMarket[0].seller).to.eq(user.address);
    });
});
