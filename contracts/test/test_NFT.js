const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("NFT 1155", function () {
    let deployer, user, user2;
    beforeEach(async function () {
        [deployer, user, user2] = await ethers.getSigners();
        const NFT = await ethers.getContractFactory("AirClass", deployer);
        this.nft = await NFT.deploy();
    });

    it("Có thể mint NFT và set URI", async function () {
        await this.nft.mint(user.address, 10, "uri1");
        expect(await this.nft.uri(1)).to.eq("uri1");
        expect(await this.nft.balanceOf(user.address, 1)).to.eq(10);

        await this.nft.mint(user.address, 10, "uri2");
        expect(await this.nft.uri(2)).to.eq("uri2");
        expect(await this.nft.balanceOf(user.address, 2)).to.eq(10);
    });

    it("Người sở hữu NFT có thể mint thêm NFT đó", async function () {
        await this.nft.mint(user.address, 10, "uri1");
        await this.nft.connect(user).increment(user.address, 1, 15);
        expect(await this.nft.balanceOf(user.address, 1)).to.eq(25);
    });

    it("Người không sở hữu NFT không thể mint thêm NFT đó", async function () {
        await this.nft.mint(user.address, 10, "uri1");
        await expect(this.nft.connect(deployer).increment(deployer.address, 1, 15)).to.revertedWith(
            "AIR CLASS: You do not own this NFT"
        );
    });

    it("Có thể Mint NFT từ testnet", async function () {
        const price = ethers.utils.parseEther("500");
        const userBalanceBefore = await ethers.provider.getBalance(user.address);
        const user2BalanceBefore = await ethers.provider.getBalance(user2.address);
        console.log("User before", ethers.utils.formatEther(userBalanceBefore));
        console.log("User2 before", ethers.utils.formatEther(user2BalanceBefore));

        await this.nft.connect(user2).mintFromTestnet(user.address, user2.address, 10, price, "uri1", { value: price });
        console.log("--------------------------------");
        const userBalanceAfter = await ethers.provider.getBalance(user.address);
        const user2BalanceAfter = await ethers.provider.getBalance(user2.address);
        console.log("User after", ethers.utils.formatEther(userBalanceAfter));
        console.log("User2 after", ethers.utils.formatEther(user2BalanceAfter));
    });
});
