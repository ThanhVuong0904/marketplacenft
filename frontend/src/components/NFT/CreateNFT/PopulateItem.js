import axios from "axios";
import React, { useState } from "react";
import { toast } from "react-toastify";


  const PopulateItem = React.memo(function MyComponents(props) {
    const [isChecked, setIsChecked] = useState(false);
  
    const backendIndia = (imageFace, imageRoot) => {
      //  Dự phòng nếu backend bị lỗi không thể trả ảnh
      console.log("Backend của anh Ấn độ");
      let convertBase64Face = imageFace.replace("data:image/jpeg;base64,", "");
      let convertBase64Root = imageRoot.replace("data:image/jpeg;base64,", "");
      let reqBody = {
        source: convertBase64Face,
        target: convertBase64Root,
        isDefault: null,
        isTest: false,
      };
      console.log("reqBody", reqBody);
      try {
        fetch(
          "https://mdii03lzf9.execute-api.us-east-2.amazonaws.com/default/simswap-lambda",
          {
            method: "POST",
            body: JSON.stringify(reqBody),
          }
        )
          .then((result) => {
            console.log("result", result);
            return result.json();
          })
          .then((data) => {
            console.log("data", data);
            let parsed = JSON.parse(data.body);
            if (parsed.img_bytes) {
              let imageFinal = "data:image/jpeg;base64," + parsed.img_bytes;
              props.setLoading(false);
              props.setResultImage(imageFinal);
              console.log(imageFinal);
              toast.success("FaceSwap Thành công!!!");
              console.log("parsed", parsed);
            } else {
              console.log("errrr");
            }
          });
      } catch (err) {
        console.log("Error: ", err);
      }
    };
  
    const swapFace = async () => {
      if (!props.account) {
        toast.warn("Vui lòng kết nối ví để thực hiện chức năng này!!!");
      } else {
        if (props.loading) {
          toast.warn("Đang thay đổi khuôn mặt!!!");
        } else {
          // Backend nếu mọi thứ chạy bình thường
          toast.success("Bắt đầu FaceSwap!!!");
          props.setLoading(true);
          let imageFace = await props.toBase64(props.selectedFileFace);
          let imageRoot = await props.toBase64(props.selectedFileRoot);
          let convertBase64Face = imageFace.replace(
            "data:image/jpeg;base64,",
            ""
          );
          let convertBase64Root = imageRoot.replace(
            "data:image/jpeg;base64,",
            ""
          );
          if (imageFace && imageRoot) {
            console.log(
              "Backend của chúng ta",
              convertBase64Face,
              convertBase64Root
            );
            await axios
              .post("https://backendpython-production-0d4e.up.railway.app", {
                base64_src: convertBase64Face,
                base64_dst: convertBase64Root,
              })
              .then((value) => {
                props.setLoading(false);
                props.setResultImage(value.data.image_url);
                console.log(value);
                toast.success("FaceSwap Thành công!!!");
              })
              .catch((err) => backendIndia(imageFace, imageRoot));
          }
        }
      }
    };
  
    const handleChecked = () => {
      if (props.selectedFileVideo) {
        toast.warn("Hiện tại chưa hỗ trợ FaceSwap video :((");
      } else {
        props.setSelectedFileFace(null);
        setIsChecked(!isChecked);
      }
    };
    const renderResult = () => {
      if (props.selectedFileFace) {
        if (props.loading) {
          return (
            <img
              className="gifLoading"
              src={
                "https://media1.giphy.com/media/l3nWhI38IWDofyDrW/giphy.gif?cid=ecf05e47zr7d94pypd8djqw70zr5xvnbgbme1loqfuxdz6qe&rid=giphy.gif&ct=g"
              }
              alt=""
            />
          );
        } else {
          return (
            <img
              src={props.resultImage}
              style={{ width: "100%", cursor: "pointer" }}
              alt=""
            />
          );
        }
      } else {
        return (
          <div className="image-upload__body">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/1156px-Picture_icon_BLACK.svg.png"
              alt=""
            />
            <p>{props.t("createNFT.result")}</p>
          </div>
        );
      }
    };
  
    return isChecked ? (
      <div className=" col-span-2">
        <div className=" grid grid-cols-3">
          <div>
            <p className="createNFT__populate-left-header">
              {props.t("createNFT.faceImg")}
            </p>
            <div className="image-upload">
              {props.selectedFileFace ? (
                <img
                  className="img-swap"
                  src={props.preview}
                  style={{ width: "100%", cursor: "pointer" }}
                  onClick={() => props.setSelectedFileFace()}
                  alt=""
                />
              ) : (
                <div className="image-upload__body">
                  <img
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/1156px-Picture_icon_BLACK.svg.png"
                    alt=""
                  />
                  <p>{props.t("createNFT.supportFile")}</p>
                  <label htmlFor="file-input">
                    <p className="btn__input">
                      {props.t("createNFT.selectFile")}
                    </p>
                  </label>
                  <input
                    id="file-input"
                    type="file"
                    onChange={(e) => props.onSelectFile(e, 0)}
                  />
                </div>
              )}
            </div>
          </div>
          <div className="wrapper wrapper-active">
            <p className="swapFace">Swap Face</p>
            <label className="switch">
              <input
                type="checkbox"
                checked={isChecked}
                onChange={handleChecked}
              />
  
              <span className="slider">
                <div className="fish">
                  <div className="body"></div>
                  <div className="eye"></div>
                  <div className="tail"></div>
                </div>
              </span>
              <span className="wave"> </span>
              <div className="boat">
                <div className="bottom"></div>
                <div className="mast"></div>
                <div className="rectangle-sm"></div>
                <div className="rectangle-lg"></div>
              </div>
            </label>
            <div className="confirm xl:col-span-3 mt-2">
              <p
                className={
                  props.selectedFileFace && props.selectedFileRoot
                    ? "btn-confirm active"
                    : "btn-confirm"
                }
                onClick={swapFace}
              >
                {props.t("createNFT.faceswap")}
              </p>
            </div>
          </div>
          <div>
            <p className="createNFT__populate-left-header">
              {props.t("createNFT.result")}
            </p>
            <div className="image-upload"> {renderResult()}</div>
          </div>
        </div>
      </div>
    ) : (
      <div className="wrapper col-span-2">
        <p className="swapFace">Swap Face</p>
        <label className="switch">
          <input type="checkbox" checked={isChecked} onChange={handleChecked} />
  
          <span className="slider">
            <div className="fish">
              <div className="body"></div>
              <div className="eye"></div>
              <div className="tail"></div>
            </div>
          </span>
          <span className="wave"> </span>
          <div className="boat">
            <div className="bottom"></div>
            <div className="mast"></div>
            <div className="rectangle-sm"></div>
            <div className="rectangle-lg"></div>
          </div>
        </label>
      </div>
    );
  });
  export default PopulateItem;