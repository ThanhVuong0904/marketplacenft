import axios from "axios";
import React, { useEffect, useState } from "react";
import { useMoralis, useMoralisFile } from "react-moralis";
import ReactPlayer from "react-player/lazy";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import PopulateItem from "./PopulateItem";

const Populate = React.memo(function MyComponent(props) {
  const { saveFile } = useMoralisFile();
  const { Moralis } = useMoralis();
  const [selectedFileFace, setSelectedFileFace] = useState();
  const [selectedFileRoot, setSelectedFileRoot] = useState();
  const [selectedFileVideo, setSelectedFileVideo] = useState();
  const [resultImage, setResultImage] = useState();
  const [preview, setPreview] = useState();
  const [previewRoot, setPreviewRoot] = useState();
  const [nameNft, setNameNft] = useState();
  const [loading, setLoading] = useState(false);
  const [opensea, setOpensea] = useState(true);
  const [linkYtb, setLinkYtb] = useState();
  const [linkImageReview, setLinkImageReview] = useState();
  const [imageURL, setImageURL] = useState();
  const [description, setDescription] = useState();
  const [content, setContent] = useState();
  const [amount, setAmount] = useState("");

  const contractEth = useSelector((state) => state.wallet.contractEth);
  const contractBsc = useSelector((state) => state.wallet.contractBsc);
  const network = useSelector((state) => state.wallet.network);
  const account = useSelector((state) => state.wallet.account);

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  useEffect(() => {
    if (!selectedFileFace) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFileFace);

    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFileFace]);

  useEffect(() => {
    if (!selectedFileRoot) {
      setPreviewRoot(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFileRoot);

    setPreviewRoot(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFileRoot]);

  const onSelectFile = (e, root) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFileFace(undefined);
      return;
    } else {
      console.log(
        'e.target.files[0].type.indexOf("video" === -1)',
        e.target.files[0].type.indexOf("video" === -1)
      );
      if (e.target.files[0].type.indexOf("video") === -1) {
        if (root === 0) {
          setSelectedFileFace(e.target.files[0]);
        } else {
          setSelectedFileRoot(e.target.files[0]);
        }
      } else {
        console.log9 = "hello";
        setSelectedFileVideo(e.target.files[0]);
      }
    }

    // I've kept this example simple by using the first image instead of multiple
  };

  const uploadAsset = async (asset) => {
    let formDataAsset = new FormData();
    formDataAsset.append("asset", asset);
    const uploadAsset = await axios.post(
      "https://api-nft.airclass.io/asset/upload",
      formDataAsset,
      {
        headers: {
          "content-type": "mutipart/form-data",
        },
      }
    );
    return uploadAsset.data.url;
  };
  const createMetadata = async (flag, linkVideo) => {
    if (flag === "image") {
      console.log("Upload Image", imageURL);
      if (imageURL === undefined) {
        //Trường hợp Image
        const asset = await uploadAsset(selectedFileRoot);
        console.log("asset", asset);
        const resMetadata = await axios.post(
          "https://api-nft.airclass.io/nft/create-metadata",
          {
            image: asset,
            name: nameNft,
            description: description,
          }
        );
        return resMetadata.data.metadata;
      } else {
        const resMetadata = await axios.post(
          "https://api-nft.airclass.io/nft/create-metadata",
          {
            image: imageURL,
            name: nameNft,
            description: description,
          }
        );
        return resMetadata.data.metadata;
      }
    }
    if (flag === "video" && linkVideo) {
      if (linkImageReview === undefined) {
        //Trường hợp Image
        const asset = await uploadAsset(imageUpload);
        console.log("asset", asset);
        const resMetadata = await axios.post(
          "https://api-nft.airclass.io/nft/create-metadata",
          {
            image: asset,
            name: nameNft,
            description: description,
            animation_url: linkVideo,
          }
        );
        return resMetadata.data.metadata;
      } else {
        const resMetadata = await axios.post(
          "https://api-nft.airclass.io/nft/create-metadata",
          {
            image: linkImageReview,
            name: nameNft,
            description: description,
            animation_url: linkVideo,
          }
        );
        return resMetadata.data.metadata;
      }
    }
  };

  useEffect(() => {
    const test = async () => {
      const a = await contractEth.methods.uri(7).call();
      console.log("uri", a);
    };
    test();
  }, []);

  const createNFT = async () => {
    if (!account) {
      toast.warn("Vui lòng đăng nhập để tạo NFT!!!");
    } else {
      console.log("loading", loading);
      if (loading) {
        toast.warn("Đang tạo NFT!!!");
      } else {
        if (opensea && network !== "rinkeby") {
          toast.warn("Vui lòng chuyển sang mạng Eth Rinkeby!!");
        } else if (!opensea && network === "rinkeby") {
          toast.warn("Vui lòng chuyển sang mạng Bsc binance!!");
        } else {
          setLoading(true);
          toast.success("Bắt đầu tạo NFT!!!");
          console.log("co goi ham nay", selectedFileRoot);
          const metadata = await createMetadata("image");

          console.log("resMetadata", metadata);
          let value = "10000000000000000";
          if (network === "rinkeby") {
            console.log(`Chay toi day v ${content}`, contractEth.methods);
            await contractEth.methods
              .mint(account, amount, metadata)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileRoot(null);
                  setSelectedFileFace(null);
                  setResultImage(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileRoot(null);
                  setSelectedFileFace(null);
                  setResultImage(null);
                }
              });
          } else {
            console.log("chay cai nay");
            await contractBsc.methods
              .mint(account, amount, metadata)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileRoot(null);
                  setSelectedFileFace(null);
                  setResultImage(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileRoot(null);
                  setSelectedFileFace(null);
                  setResultImage(null);
                }
              });
          }
        }
      }
    }
  };

  const uploadVideo = async () => {
    if (!account) {
      toast.warn("Vui lòng đăng nhập để tạo NFT!!!");
    } else {
      console.log("opensea && network", opensea, network);
      if (opensea && network !== "rinkeby") {
        toast.warn("Vui lòng chuyển sang mạng Eth Rinkeby!!");
      } else if (!opensea && network === "rinkeby") {
        toast.warn("Vui lòng chuyển sang mạng Bsc binance!!");
      } else {
        if (!linkYtb) {
          toast.success("Bắt đầu tạo NFT!!!");
          setLoading(true);
          const asset = await uploadAsset(selectedFileVideo);
          console.log(asset);
          const resMetadata = await axios.post(
            "https://api-nft.airclass.io/nft/create-metadata",
            {
              image: imageURL,
              name: nameNft,
              description: description,
              animation_url: asset,
            }
          );
          if (network === "rinkeby") {
            await contractEth.methods
              .mint(resMetadata.data.url)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                  setResultImage(null);
                }
              });
          } else {
            await contractBsc.methods
              .mint(resMetadata.data.url)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                  setResultImage(null);
                }
              });
          }
        } else {
          const metadata = await createMetadata("video", linkYtb);
          console.log({ metadata });
          if (network === "rinkeby") {
            await contractEth.methods
              .mint(account, amount, metadata)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                  setResultImage(null);
                }
              });
          } else {
            await contractBsc.methods
              .mint(account, amount, metadata)
              .send({ from: account }, function (err, res) {
                if (res) {
                  toast.success("Thêm NFT thành công!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                } else {
                  toast.warn("Thêm NFT thất bại!!!");
                  setLoading(false);
                  setSelectedFileVideo(null);
                  setResultImage(null);
                }
              });
          }
        }
      }
    }
  };

  const renderRootReview = () => {
    if (loading && !selectedFileFace) {
      return (
        <img
          className="img-swap"
          src={
            "https://media1.giphy.com/media/l3nWhI38IWDofyDrW/giphy.gif?cid=ecf05e47zr7d94pypd8djqw70zr5xvnbgbme1loqfuxdz6qe&rid=giphy.gif&ct=g"
          }
          alt=""
        />
      );
    } else {
      if (selectedFileVideo) {
        let src = URL.createObjectURL(selectedFileVideo);
        return (
          <ReactPlayer
            className="img-swap"
            url={src}
            playing={false}
            controls={true}
            onClick={() => setSelectedFileVideo(null)}
          />
        );
      } else {
        if (selectedFileRoot || imageURL) {
          return (
            <img
              className="img-swap"
              src={imageURL ? imageURL : previewRoot}
              style={{ width: "100%", cursor: "pointer" }}
              onClick={() => setSelectedFileRoot()}
              alt=""
            />
          );
        } else {
          return (
            <div className="image-upload__body">
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/1156px-Picture_icon_BLACK.svg.png"
                alt=""
              />
              <p>{props.t("createNFT.supportFile")}</p>
              <label htmlFor="file-input">
                <p className="btn__input"> {props.t("createNFT.selectFile")}</p>
              </label>
              <input
                id="file-input"
                type="file"
                onChange={(e) => onSelectFile(e, 1)}
              />
            </div>
          );
        }
      }
    }
  };

  const renderYoutubeReview = () => {
    if (loading && !selectedFileFace) {
      return (
        <img
          className="img-swap"
          src={
            "https://media1.giphy.com/media/l3nWhI38IWDofyDrW/giphy.gif?cid=ecf05e47zr7d94pypd8djqw70zr5xvnbgbme1loqfuxdz6qe&rid=giphy.gif&ct=g"
          }
          alt=""
        />
      );
    } else {
      if (linkYtb) {
        return (
          <ReactPlayer
            className="img-swap"
            url={linkYtb}
            playing={false}
            controls={true}
            onClick={() => setSelectedFileVideo(null)}
          />
        );
      } else {
        if (selectedFileRoot) {
          return (
            <img
              className="img-swap"
              src={previewRoot}
              style={{ width: "100%", cursor: "pointer" }}
              onClick={() => setSelectedFileRoot()}
              alt=""
            />
          );
        } else {
          return (
            <div className="image-upload__body">
              <img
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Picture_icon_BLACK.svg/1156px-Picture_icon_BLACK.svg.png"
                alt=""
              />
              <p>Review video Youtube</p>
            </div>
          );
        }
      }
    }
  };

  // Vuong test
  const [imageUpload, setImageUpload] = useState("");

  const handleChangeUploadImage = (e) => {
    const file = e.target.files[0];
    setImageUpload(file);
  };

  return (
    <div className="createNFT__buy">
      <div className="createNFT__populate-left grid grid-cols-1 sm:grid-cols-3 gap-3">
        <div>
          <p className="createNFT__populate-left-header">
            {props.t("createNFT.rootImg")}
          </p>
          <div className="image-upload">
            {props.statusCheck === "Single"
              ? renderRootReview()
              : renderYoutubeReview()}
          </div>
        </div>
        <PopulateItem
          selectedFileFace={selectedFileFace}
          selectedFileVideo={selectedFileVideo}
          loading={loading}
          setLoading={setLoading}
          resultImage={resultImage}
          preview={preview}
          setSelectedFileFace={setSelectedFileFace}
          onSelectFile={onSelectFile}
          t={props.t}
          selectedFileRoot={selectedFileRoot}
          account={account}
          toBase64={toBase64}
          setResultImage={setResultImage}
          saveFile={saveFile}
        />
      </div>

      <div className="createNFT__populate-right">
        <from>
          <div className="flex grid xl:grid-cols-2 gap-3">
            <div className="createNFT__populate-right-item ">
              <p className="populate-right-header">
                {props.t("createNFT.NFTName")}
              </p>
              <input
                value={nameNft}
                type="text"
                name="nameNft"
                placeholder={props.t("createNFT.NFTNameInput")}
                onChange={(e) => setNameNft(e.target.value)}
              />
            </div>
            <div className="createNFT__populate-right-item ">
              <p className="populate-right-header text-center">Opensea</p>
              <div>
                <img
                  onClick={() => setOpensea(!opensea)}
                  className="mt-2 flex justify-center mx-auto"
                  src="https://opensea.io/static/images/logos/opensea.svg"
                  alt=""
                  style={
                    opensea
                      ? {
                          height: "42px",
                          cursor: "pointer",
                        }
                      : {
                          height: "42px",
                          opacity: "0.6",
                          cursor: "pointer",
                        }
                  }
                />
                <p className="mt-2 flex justify-center">
                  {opensea
                    ? "Xuất hiện trên sàn Opensea"
                    : "Không xuất hiện trên sàn Opensea"}
                </p>
              </div>
            </div>
          </div>

          <div className="createNFT__populate-right-item default">
            <p className="populate-right-header">
              {props.t("createNFT.vaultName")}
            </p>
            <input
              type="text"
              placeholder="Please enter the name"
              value="AIRCLASS"
              disabled
            />
          </div>
          <div className="createNFT__populate-right-item default">
            <p className="populate-right-header">Description</p>
            <input
              type="text"
              placeholder="Link image review"
              name="description"
              value={description}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            />
          </div>
          <div className="createNFT__populate-right-item default">
            <p className="populate-right-header">Content</p>
            <input
              type="text"
              placeholder="Link image review"
              name="content"
              value={content}
              onChange={(e) => {
                setContent(e.target.value);
              }}
            />
          </div>
          <div className="createNFT__populate-right-item default">
            <p className="populate-right-header">Amount</p>
            <input
              type="text"
              placeholder="Số lượng nft"
              name="amount"
              value={amount}
              onChange={(e) => {
                setAmount(e.target.value);
              }}
            />
          </div>

          {props.statusCheck !== "Multiple" && (
            <div className="createNFT__populate-right-item ">
              <p className="populate-right-header">Image URL</p>
              <input
                type="text"
                placeholder="NFT Image Url"
                name="imageURL"
                value={imageURL}
                onChange={(e) => {
                  setImageURL(e.target.value);
                }}
              />
            </div>
          )}

          {props.statusCheck === "Multiple" && (
            <div>
              <div className="createNFT__populate-right-item ">
                <p className="populate-right-header">Link Image Review</p>
                <input
                  type="text"
                  placeholder="Link image review"
                  name="linkImageReview"
                  value={linkImageReview}
                  onChange={(e) => {
                    setLinkImageReview(e.target.value);
                  }}
                />
              </div>
              <div className="createNFT__populate-right-item ">
                <p className="populate-right-header">Upload Image Review</p>
                <input type="file" onChange={handleChangeUploadImage} />
              </div>

              <div className="createNFT__populate-right-item ">
                <p className="populate-right-header">Link youtube</p>
                <input
                  type="text"
                  placeholder="Link video youtube"
                  name="linkYtb"
                  value={linkYtb}
                  onChange={(e) => {
                    setLinkYtb(e.target.value);
                  }}
                />
              </div>
            </div>
          )}
        </from>
      </div>
      <div className="confirm xl:col-span-3 ">
        <p
          className={
            (nameNft && selectedFileRoot) ||
            selectedFileVideo ||
            linkYtb ||
            imageURL
              ? "btn-confirm active"
              : "btn-confirm"
          }
          onClick={selectedFileVideo || linkYtb ? uploadVideo : createNFT}
        >
          {props.t("createNFT.submit")}
        </p>
      </div>
    </div>
  );
});
export default Populate;
