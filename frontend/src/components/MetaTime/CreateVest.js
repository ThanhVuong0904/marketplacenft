import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { toast } from "react-toastify";
import { FaArrowUp } from "react-icons/fa";
import { ethers } from "moralis/node_modules/ethers";
import actionTypes from "../../store/constants/actionTypes";

const CreateStakeStyled = styled.div`
  .staking {
    min-height: calc(100vh);
    background-color: ${(props) => props.theme.bgHeader};

    .staking-top {
      padding: 40px 0;
      background-color: ${(props) => props.theme.bgHeaderItem};
      min-height: calc(100vh);
      .staking__top {
        width: calc(100vw - 312px);
        max-width: 1160px;
        margin: 0 auto;
        color: ${(props) => props.theme.textBody};

        .staking__top-title {
          font-size: 36px;
          font-weight: bold;
          margin-bottom: 20px;
        }
        #select-profession {
          color: black;
          padding: 18px;
          border-radius: 12px;
          font-size: 18px;
          line-height: 28px;
          width: 80%;
        }

        .select__typecoin {
          padding: 48px;
          border: 1px solid #ccc;
          border-radius: 12px;
          .mynft__btn-right-body {
            margin: 18px 0;
            .stake__now:hover {
              opacity: 0.8;
              cursor: pointer;
            }
            p {
              line-height: 56px;
              font-size: 18px;
            }
            .stake__now {
              width: 885px;
              height: 74px;
              left: 171px;
              top: 896px;
              background: #f9d205;
              border-radius: 20px;
              text-align: center;
              line-height: 74px;
              color: black;
              font-weight: bold;
              margin: 18px auto 0 auto;
            }
          }
        }
      }
    }
  }

  @media screen and (max-width: 1023px) {
    .staking {
      width: 100%;
      .staking-top {
        .staking__top {
          padding: 12px;
          min-width: calc(100vw - 180px);
        }
      }
      .staking-bot {
        .staking__bot {
          padding: 12px;
          min-width: calc(100vw - 200px);
        }
      }
    }
  }
  @media screen and (max-width: 767px) {
    .staking {
      width: 100%;
      .staking-top {
        .staking__top {
          padding: 12px;
          width: 100%;
        }
      }
    }
  }
`;
export default function CreateVest() {
  const { t } = useTranslation();
  const [currentTypeCoin, setCurrentTypeCoin] = useState("Ethereum Testnet");
  const [numberCoin, setNumberCoin] = useState("");
  const [timeUnlock, setTimeUnlock] = useState(1);

  const sortTime = [1, 3, 6, 9, 12, 18, 24, 36];
  const [timeStaking, setTimeStaking] = useState([]);
  const [balanceDak, setBalanceDak] = useState(0);
  const [mintDak, setMintDak] = useState("");

  const account = useSelector((state) => state.wallet.account);

  const network = useSelector((state) => state.wallet.network);
  const itemStake = useSelector((state) => state.home.itemStake);
  console.log("itemStake", itemStake);
  const contractVestingETH = useSelector(
    (state) => state.wallet.contractVestingETH
  );
  const contractVestingBSC = useSelector(
    (state) => state.wallet.contractVestingBSC
  );
  const Erc20ETH = useSelector((state) => state.wallet.Erc20ETH);
  const Erc20BSC = useSelector((state) => state.wallet.Erc20BSC);

  const contractStakingBSC = useSelector(
    (state) => state.wallet.contractStakingBSC
  );

  useEffect(() => {
    async function fetchData() {
      sortTime.forEach((item) => {
        if (item === +itemStake.time / 60) {
          setTimeStaking((timeStaking) => [...timeStaking, item]);
        }
      });
      let erc20;
      if (network === "rinkeby") {
        erc20 = Erc20ETH;
      } else {
        erc20 = Erc20BSC;
      }
      let balance = account ? await erc20.methods.balanceOf(account).call() : 0;
      const gweiValue = ethers.utils.formatUnits(balance, "ether");
      setBalanceDak(gweiValue);
      console.log("balance", balance, gweiValue);
    }
    // console.log(contractStakingETH, account);
    Erc20BSC && Erc20ETH && fetchData();
  }, [Erc20BSC, Erc20ETH, account, network]);

  const createStaking = async () => {
    console.log("currentTypeCoin", currentTypeCoin, numberCoin);
    console.log("network", network);
    if (itemStake.typeCoin === "ETH" && network !== "rinkeby") {
      toast.warn("Vui lòng chọn lại mạng ETH!");
    } else if (itemStake.typeCoin === "BSC" && network !== "bsc") {
      toast.warn("Vui lòng chọn lại mạng BSC");
    } else {
      let contract;
      let contractERC20;
      if (network === "rinkeby") {
        contract = contractVestingETH;
        contractERC20 = Erc20ETH;
      } else {
        contract = contractVestingBSC;
        contractERC20 = Erc20BSC;
      }
      let coinType = network === "rinkeby" ? "ETH" : "BSC";
      let value = ethers.utils.parseUnits(numberCoin, 18);
      let time = +timeUnlock * 60;
      console.log(value.toString(), itemStake.id);
      await contractERC20.methods
        .approve(
          network === "rinkeby"
            ? actionTypes.ADDRESS_VESTING_ETH
            : actionTypes.ADDRESS_VESTING_BSC,
          value.toString()
        )
        .send({ from: account, gas: "300000" }, async (err, res) => {
          if (res) {
            toast.success("Approve thành công!!!");
            numberCoin
              ? await contract.methods
                  .createStake(value.toString(), itemStake.id)
                  .send({ from: account, gas: "300000" }, function (err, res) {
                    if (res) {
                      toast.success("Tham gia vesting thành công!!!");
                    } else {
                      toast.warn("Tham gia vesting thất bại!!!");
                    }
                  })
              : toast.warn("Vui lòng nhập lãi suất!");
          } else {
            toast.warn("Approve thất bại!!!");
          }
        });
    }
  };

  const mintDakToken = async () => {
    let contract;
    if (network === "rinkeby") {
      contract = Erc20ETH;
    } else {
      contract = Erc20BSC;
    }
    await contract.methods
      .mintMinerReward(mintDak)
      .send({ from: account, gas: "300000" }, async (err, res) => {
        if (res) {
          toast.success("MintDak thành công!!!");
        } else {
          toast.warn("MintDak  thất bại!!!");
        }
      });
  };

  return (
    <CreateStakeStyled>
      <div className="staking">
        <div className="staking-top ">
          <div className="staking__top">
            <h1 className="staking__top-title">Vesting</h1>
            <div className="select__typecoin">
              <p className="text-center">DAK Đang sở hữu: {balanceDak} DAK</p>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Loại tiền ảo</p>
                <select
                  name="select-profession"
                  id="select-profession"
                  value={currentTypeCoin}
                  onChange={(e) => setCurrentTypeCoin(e.target.value)}
                >
                  <option
                    className="option"
                    value={itemStake.typeCoin}
                    key={itemStake.typeCoin}
                  >
                    {itemStake.typeCoin === "BSC"
                      ? "Binance Smart Chain Testnet"
                      : "Ethereum Testnet"}
                  </option>
                </select>
              </div>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Số tiền cần Vesting</p>
                <input
                  type="number"
                  placeholder="Nhập số lượng coin stake"
                  name="select-profession"
                  id="select-profession"
                  value={numberCoin}
                  onChange={(e) => {
                    let value = e.target.value;
                    value.charCodeAt(numberCoin.length) !== 46 &&
                      setNumberCoin(e.target.value);
                  }}
                />
              </div>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Kỳ hạn</p>
                <p
                  className="name__select text-center flex-1"
                  style={{ fontWeight: "bold" }}
                >
                  {Math.floor(itemStake.time / 60 / 1440)} ngày{" "}
                  {Math.floor(
                    (itemStake.time / 60 -
                      Math.floor(itemStake.time / 60 / 1440) * 1440) /
                      60
                  )}{" "}
                  giờ {Math.round((itemStake.time / 60) % 60)} phút
                </p>
              </div>

              <div className=" mynft__btn-right-body">
                <p>Phần trăm lãi suất (% / {+itemStake.time / 60} tháng)</p>
                <div className="flex ">
                  <p
                    style={{
                      width: "50%",
                      color: "#F9D205",
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                  >
                    {itemStake.interestRate} %
                  </p>

                  <p
                    style={{
                      width: "50%",
                      color: " #349B24",
                      fontSize: "24px",
                      fontWeight: "bold",
                    }}
                    className="flex"
                  >
                    <FaArrowUp style={{ margin: "auto 12px" }} /> 10 %
                  </p>
                </div>
              </div>
              <div className=" mynft__btn-right-body">
                <p>Lãi suất (DAK Token)</p>
              </div>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="stake__now" onClick={createStaking}>
                  Xác nhận
                </p>
              </div>
            </div>
            <div className=" mynft__btn-right-body mt-6 flex justify-around">
              <p className="name__select" style={{ fontWeight: "bold" }}>
                Số DAK nhận{" "}
              </p>
              <input
                type="number"
                placeholder="Nhập số lượng coin stake"
                name="select-profession"
                id="select-profession"
                value={mintDak}
                onChange={(e) => {
                  let value = e.target.value;
                  value.charCodeAt(mintDak.length) !== 46 &&
                    setMintDak(e.target.value);
                }}
              />
              <p
                className="stake__now "
                style={{
                  width: "200px",
                  height: "74px",
                  left: "171px",
                  top: "896px",
                  background: "#f9d205",
                  borderRadius: "20px",
                  textAlign: "center",
                  lineHeight: "74px",
                  color: "black",
                  fontWeight: "bold",
                  margin: "18px auto 0 auto",
                  cursor: "pointer",
                }}
                onClick={mintDakToken}
              >
                Xác nhận
              </p>
            </div>
          </div>
        </div>
      </div>
    </CreateStakeStyled>
  );
}
