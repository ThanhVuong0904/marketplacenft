import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { toast } from "react-toastify";

const CreateFragmentStyled = styled.div`
  .staking {
    min-height: calc(100vh);
    background-color: ${(props) => props.theme.bgHeader};

    .staking-top {
      padding: 40px 0;
      background-color: ${(props) => props.theme.bgHeaderItem};
      min-height: calc(100vh);
      .staking__top {
        width: calc(100vw - 312px);
        max-width: 1160px;
        margin: 0 auto;
        color: ${(props) => props.theme.textBody};

        .staking__top-title {
          font-size: 36px;
          font-weight: bold;
          margin-bottom: 20px;
        }
        #select-profession {
          color: black;
          padding: 18px;
          border-radius: 12px;
          font-size: 18px;
          line-height: 28px;
          width: 80%;
        }

        .select__typecoin {
          padding: 36px;
          border: 1px solid #ccc;
          border-radius: 12px;
          .mynft__btn-right-body {
            margin: 18px 0;
            .stake__now:hover {
              opacity: 0.8;
              cursor: pointer;
            }
            p {
              line-height: 56px;
              font-size: 18px;
            }
            .stake__now {
              width: 885px;
              height: 74px;
              left: 171px;
              top: 896px;
              background: #f9d205;
              border-radius: 20px;
              text-align: center;
              line-height: 74px;
              color: black;
              margin: 18px auto 0 auto;
            }
          }
        }
      }
    }
  }

  @media screen and (max-width: 1023px) {
    .staking {
      width: 100%;
      .staking-top {
        .staking__top {
          padding: 12px;
          min-width: calc(100vw - 180px);
        }
      }
      .staking-bot {
        .staking__bot {
          padding: 12px;
          min-width: calc(100vw - 200px);
        }
      }
    }
  }
  @media screen and (max-width: 767px) {
    .staking {
      width: 100%;
      .staking-top {
        .staking__top {
          padding: 12px;
          width: 100%;
        }
      }
    }
  }
`;
export default function CreateVesting() {
  const { t } = useTranslation();
  const [currentTypeCoin, setCurrentTypeCoin] = useState("Ethereum Testnet");
  const [currentTime, setCurrentTime] = useState("");
  const [interestRate, setInterestRate] = useState("");
  const [timeUnlock, setTimeUnlock] = useState("");

  const [addressToken, setAddressToken] = useState("");
  const sortCoin = ["Ethereum Testnet", "Binance Smart Chain Testnet"];

  const account = useSelector((state) => state.wallet.account);

  const network = useSelector((state) => state.wallet.network);
  const contractVestingETH = useSelector(
    (state) => state.wallet.contractVestingETH
  );

  const contractVestingBSC = useSelector(
    (state) => state.wallet.contractVestingBSC
  );

  useEffect(() => {
    async function fetchData() {
      console.log("contractStakingETH", contractVestingETH);
    }
    // console.log(contractStakingETH, account);
    contractVestingETH && fetchData();
  }, [contractVestingETH]);

  const createVesting = async () => {
    if (!account) {
      toast.warn("Vui lòng đăng nhập!!!");
    }
    if (currentTypeCoin === "Ethereum Testnet" && network !== "rinkeby") {
      toast.warn("Vui lòng chọn lại mạng ETH!");
    } else if (
      currentTypeCoin === "Binance Smart Chain Testnet" &&
      network !== "bsc"
    ) {
      toast.warn("Vui lòng chọn lại mạng BSC");
    } else {
      let contract;
      if (network === "rinkeby") {
        contract = contractVestingETH;
      } else {
        contract = contractVestingBSC;
      }
      let coinType = network === "rinkeby" ? "ETH" : "BSC";
      let time = +currentTime * 60;
      console.log("time, timeUnlock, coinType", time, interestRate, timeUnlock);

      time && interestRate
        ? await contract.methods
            .createVesting(time, interestRate, coinType)
            .send({ from: account, gas: "300000" }, function (err, res) {
              if (res) {
                toast.success("Khởi tạo staking thành công!!!");
              } else {
                toast.warn("Khởi tạo staking thất bại!!!");
              }
            })
        : toast.warn("Vui lòng nhập lãi suất!");
    }
  };

  return (
    <CreateFragmentStyled>
      <div className="staking">
        <div className="staking-top ">
          <div className="staking__top">
            <h1 className="staking__top-title">Tạo Vesting</h1>
            <div className="select__typecoin">
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Address</p>
                <input
                  type="text"
                  placeholder="Nhập thời gian nhận lãi suất"
                  name="select-profession"
                  id="select-profession"
                  value={timeUnlock}
                  onChange={(e) => {
                    let value = e.target.value;
                    value.charCodeAt(timeUnlock.length) !== 46 &&
                      setTimeUnlock(e.target.value);
                  }}
                />
              </div>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Mạng</p>
                <select
                  name="select-profession"
                  id="select-profession"
                  value={currentTypeCoin}
                  onChange={(e) => setCurrentTypeCoin(e.target.value)}
                >
                  {sortCoin.map((item) => {
                    return (
                      <option className="option" value={item} key={item}>
                        {item}
                      </option>
                    );
                  })}
                </select>
              </div>

              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Kỳ hạn</p>
                <input
                  type="number"
                  placeholder="Nhập số phút"
                  name="select-profession"
                  id="select-profession"
                  value={currentTime}
                  onChange={(e) => {
                    let value = e.target.value;
                    value.charCodeAt(currentTime.length) !== 46 &&
                      setCurrentTime(e.target.value);
                  }}
                />
              </div>
              <div className=" mynft__btn-right-body flex justify-center">
                <p className="name__select">
                  Thời gian hiện tại: {Math.floor(currentTime / 1440)} ngày{" "}
                  {Math.floor(
                    (currentTime - Math.floor(currentTime / 1440) * 1440) / 60
                  )}{" "}
                  giờ {Math.round(currentTime % 60)} phút
                </p>
              </div>
              <div className=" mynft__btn-right-body flex justify-between">
                <p className="name__select">Lãi suất (%)</p>
                <input
                  type="number"
                  placeholder="Nhập lãi suất"
                  name="select-profession"
                  id="select-profession"
                  value={interestRate}
                  onChange={(e) => {
                    let value = e.target.value;
                    value.charCodeAt(interestRate.length) !== 46 &&
                      setInterestRate(e.target.value);
                  }}
                />
              </div>

              <div className=" mynft__btn-right-body flex justify-between">
                <p className="stake__now" onClick={createVesting}>
                  Tạo Vesting ngay!!
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </CreateFragmentStyled>
  );
}
