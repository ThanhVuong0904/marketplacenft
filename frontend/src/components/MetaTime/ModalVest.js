import { useEffect, useState } from "react";
import { AiFillCloseCircle } from "react-icons/ai";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import LogoAirClass from '../../logo/logo (1).png'

export default function ModalVest(props) {
  const [amount, setAmount] = useState(null);
  const [maxReward, setMaxReward] = useState(0);
  const [priceETH, setPriceETH] = useState();
  const [priceDAK, setPriceDAK] = useState();
  const [currentReward, setCurrentReward] = useState();
  const [currentUnlock, setCurrentUnlock] = useState();
  const [timeUnlock, setTimeUnlock] = useState([]);

  const [activeStep, setActiveStep] = useState(0);
  const account = useSelector((state) => state.wallet.account);
  const network = useSelector((state) => state.wallet.network);
  const contractVestingETH = useSelector(
    (state) => state.wallet.contractVestingETH
  );
  const contractStakingBSC = useSelector(
    (state) => state.wallet.contractStakingBSC
  );

  const steps = [
    {
      label: `Số lần nhận token thưởng còn lại ${currentUnlock}`,
      description: `Bạn sẽ nhận được ${currentReward} DAK`,
    },

    {
      label: "Nhận coin đợt cuối",
      description: `Bạn sẽ nhận được  ${currentReward} DAK còn lại và lượng coin bạn đã staking`,
    },
  ];

  const rewardVesting = async () => {
    let contract;
    if (network === "rinkeby") {
      contract = contractVestingETH;
    } else {
      contract = contractStakingBSC;
    }
    let currentUnlock = await contract.methods
      .timeStaked(account, props.currentItem.id)
      .call();
    console.log("currentUnlock", currentUnlock);
    if (+currentUnlock < Date.now() / 1000) {
      await contract.methods
        .withdrawReward(props.currentItem.id)
        .send({ from: account, gas: "300000" }, function (err, res) {
          if (res) {
            toast.success("Nhận Reward thành công!!!");
            props.setShowModal(false);
          } else {
            toast.warn("Staking thất bại!!!");
          }
        });
    } else {
      toast.warn("Bạn chưa thể đủ thời gian để nhận mốc này!");
    }
  };



  const renderReward = () => {
    return (
      <div className="meta__modal-body">
        <div className=" meta__modal-body-item">
          <div className=" meta__modal-body-item">
            <div style={{ marginLeft: "12px", textAlign: "center" }}>
              <p>
                <b>Thời gian hoàn thành stake</b>
              </p>

              <b>{props.currentItem.status}</b>
            </div>
          </div>
          <div className="flex">
            <img
              src={
                props.currentItem.typeCoin === "ETH"
                  ? "https://s2.coinmarketcap.com/static/img/coins/200x200/1027.png"
                  : "https://i.pinimg.com/564x/9d/da/7b/9dda7b9b586a0ea8ee40ae978005d9fb.jpg"
              }
              alt=""
            />
            <div>
              <p>Số lượng Staked {props.currentItem.typeCoin} của bạn</p>
              <p>
                <b>{props.currentItem.balanceStaking} ETH</b>
              </p>
            </div>
          </div>
        </div>

        <div className="flex meta__modal-body-item">
          <div className="flex">
            <img
              src={LogoAirClass}
              alt=""
            />
            <div>
              <p>DAK thưởng</p>
              <p>
                <b>{props.currentItem.balanceReward} DAK</b>
              </p>
            </div>
          </div>
        </div>
        <p className="btn__enable" onClick={() => rewardVesting()}>
          Nhận thưởng
        </p>
      </div>
    );
  };

  return (
    <>
      <div className="meta__modal">
        <div className="meta__modal-header flex ">
          <h4>{props.currentStatus === "view" ? "Chi tiết" : "Reward"}</h4>
          <AiFillCloseCircle
            className="header-icon"
            onClick={() => props.setShowModal(false)}
          />
        </div>
        {props.currentStatus === "view" ? (
          <div className="meta__modal-body">
            <div className=" meta__modal-body-item">
              <div className=" meta__modal-body-item">
                <div style={{ marginLeft: "12px", textAlign: "center" }}>
                  <p>
                    <b>Thời gian hoàn thành stake</b>
                  </p>

                  <b>{props.currentItem.status}</b>
                </div>
              </div>
              <div className="flex">
                <img
                  src={
                    props.currentItem.typeCoin === "ETH"
                      ? "https://s2.coinmarketcap.com/static/img/coins/200x200/1027.png"
                      : "https://i.pinimg.com/564x/9d/da/7b/9dda7b9b586a0ea8ee40ae978005d9fb.jpg"
                  }
                  alt=""
                />
                <div>
                  <p>Số lượng Staked {props.currentItem.typeCoin} của bạn</p>
                  <p>
                    <b>{props.currentItem.balanceStaking} ETH</b>
                  </p>
                </div>
              </div>
            </div>

            <div className="flex meta__modal-body-item">
              <div className="flex">
                <img
                  src={LogoAirClass}
                  alt=""
                />
                <div>
                  <p>DAK thưởng</p>
                  <p>
                    <b>{props.currentItem.balanceReward} DAK</b>
                  </p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          renderReward()
        )}
      </div>
      <div
        className="opacity-25 fixed inset-0 z-40 bg-black"
        onClick={() => props.setShowModal(false)}
      ></div>
    </>
  );
}
